﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;

namespace Login_Server.Packets.Client
{
    public class LoginPacket
    {
        public string Account;
        public string Password;


        public static LoginPacket Parse(NetworkMessage message)
        {
            LoginPacket packet = new LoginPacket();
            packet.Account = message.GetString();
            packet.Password = message.GetString();
            return packet;
        }
    }
}

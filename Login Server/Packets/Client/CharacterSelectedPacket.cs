﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;

namespace Login_Server.Packets.Client
{
    public class CharacterSelectedPacket
    {
        public uint CharacterId;

        public static CharacterSelectedPacket Parse(NetworkMessage message)
        {
            CharacterSelectedPacket packet = new CharacterSelectedPacket();
            packet.CharacterId = message.GetUInt32();

            return packet;
        }
    }
}

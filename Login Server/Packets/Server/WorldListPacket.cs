﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;

namespace Login_Server.Packets.Server
{
    public class WorldListPacket
    {
        public List<string> WorldsName = new List<string>();
        public List<string> WorldsIP = new List<string>();
        public List<int> WorldsPort = new List<int>();

        public void Add(NetworkMessage message)
        {
            message.AddByte((byte)MessageType.WordListPacket);

            message.AddUInt16((ushort)WorldsName.Count);

            for (int i = 0; i < WorldsName.Count; i++)
            {
                message.AddString(WorldsName[i]);
                message.AddString(WorldsIP[i]);
                message.AddUInt32((uint)WorldsPort[i]);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;

namespace Login_Server.Packets.Server
{
    public class CharacterListPacket
    {
        public Dictionary<int, string> CharacterList = new Dictionary<int, string>();

        public void Add(NetworkMessage message)
        {
            if (CharacterList.Count <= 0)
            {
                return;
            }

            message.AddByte((byte)MessageType.CharacterListPacket);

            message.AddUInt16((ushort)CharacterList.Count);

            for (int i = 0; i < CharacterList.Count; i++)
            {
                message.AddUInt32((uint)CharacterList.ElementAt(i).Key);
                message.AddString(CharacterList.ElementAt(i).Value);
            }
        }
    }
}

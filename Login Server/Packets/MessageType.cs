﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Login_Server.Packets
{
    public enum MessageType : byte
    {
        LoginPacket = 0x01,
        CharacterListPacket = 0x02,
        CharacterSelectedPacket = 0x03,
        WordListPacket = 0x04
    }
}

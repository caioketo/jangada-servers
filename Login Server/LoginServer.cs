﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;
using Login_Server.Packets;
using Login_Server.Packets.Client;
using Login_Server.Packets.Server;

namespace Login_Server
{
    public class LoginServer
    {
        static void Main(string[] args)
        {
            new LoginServer().Run();
        }


        Server server;
        Config config;

        void Run()
        {
            config = new Config();
            config.Load();

            server = new Server(7070, 7272, ParsePacket, null, null);
            server.Run();


            while (true)
            {
                string line = Console.ReadLine();

                if (line == "exit")
                {
                    break;
                }
            }
        }


        public void ParsePacket(byte type, NetworkMessage message, Connection connection)
        {
            NetworkMessage outMessage = new NetworkMessage();
            switch ((MessageType)type)
            {
                case MessageType.LoginPacket :
                    LoginPacket packet = LoginPacket.Parse(message);

                    //Validate account on Database if valid returns characterList
                    int accountId = Database.ValidateAcc(packet.Account, packet.Password);
                    if (accountId > 0)
                    {
                        CharacterListPacket outPacket = new CharacterListPacket();
                        outPacket.CharacterList = Database.GetCharacterList(accountId);
                        outPacket.Add(outMessage);
                        connection.Send(outMessage);
                    }

                    break;
                case MessageType.CharacterSelectedPacket :
                    CharacterSelectedPacket charPacket = CharacterSelectedPacket.Parse(message);

                    WorldListPacket worldPacket = new WorldListPacket();
                    worldPacket.WorldsName = config.WorldsName;
                    worldPacket.WorldsIP = config.WorldsIP;
                    if (connection.isWeb)
                    {
                        worldPacket.WorldsPort = config.WorldsWebPort;
                    }
                    else
                    {
                        worldPacket.WorldsPort = config.WorldsPort;
                    }
                    worldPacket.Add(outMessage);
                    connection.Send(outMessage);

                    break;
            }
        }

    }
}

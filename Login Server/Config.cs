﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Login_Server
{
    public class Config
    {
        public List<string> WorldsName;
        public List<string> WorldsIP;
        public List<int> WorldsPort;
        public List<int> WorldsWebPort;

        public Config()
        {
            WorldsName = new List<string>();
            WorldsIP = new List<string>();
            WorldsPort = new List<int>();
            WorldsWebPort = new List<int>();
        }

        public void Load()
        {
            //Load from file
            WorldsName.Add("Jangada");
            WorldsIP.Add("jangadaserver.no-ip.info");
            WorldsPort.Add(7171);
            WorldsWebPort.Add(7373);
            WorldsName.Add("Jangada Local (Apenas Eu)");
            WorldsIP.Add("192.168.25.2");
            WorldsPort.Add(7171);
            WorldsWebPort.Add(7373);
        }
    }
}

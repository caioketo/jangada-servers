﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JSLib
{
    public enum Direction : byte
    {
        North = 0x00,
        East = 0x01,
        South = 0x02,
        West = 0x03,
        NorthWest = 0x04,
        SouthWest = 0x05,
        NorthEast = 0x06,
        SouthEast = 0x07
    }
}

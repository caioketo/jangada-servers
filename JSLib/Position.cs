﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JSLib
{
    public class Position : IEquatable<Position>
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        public Position(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public Position(Position pos)
        {
            X = pos.X;
            Y = pos.Y;
            Z = pos.Z;
        }

        public override bool Equals(object other)
        {
            return other is Position && Equals((Position)other);
        }

        public bool Equals(Position other)
        {
            return other.X == X && other.Y == Y && other.Z == Z;
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + X.GetHashCode();
            hash = hash * 23 + Y.GetHashCode();
            hash = hash * 23 + Z.GetHashCode();
            return hash;
        }

        public Position Offset(Direction direction)
        {
            return Offset(direction, 1);
        }

        public Position Offset(Direction direction, int amount)
        {
            double x = X, y = Y, z = Z;

            switch (direction)
            {
                case Direction.North:
                    y -= amount;
                    break;
                case Direction.South:
                    y += amount;
                    break;
                case Direction.West:
                    x -= amount;
                    break;
                case Direction.East:
                    x += amount;
                    break;
                case Direction.NorthWest:
                    x -= amount;
                    y -= amount;
                    break;
                case Direction.SouthWest:
                    x -= amount;
                    y += amount;
                    break;
                case Direction.NorthEast:
                    x += amount;
                    y -= amount;
                    break;
                case Direction.SouthEast:
                    x += amount;
                    y += amount;
                    break;
            }

            return new Position(x, y, z);
        }

        public bool CanSee(Position pos)
        {
            if (Z != pos.Z)
            {
                return false;
            }

            return ((pos.X >= X - 14) && (pos.X <= X + 15) && (pos.Y >= Y - 10) && (pos.Y <= Y + 11));
        }

        public bool isInArea(Position[] Area)
        {
            bool result = false;

            for (int i = 0; i < Area.Length; i++)
            {
                if (this.Equals(Area[i]))
                {
                    result = true;
                    break;
                }
            }

            return result;
        }

        public bool isInRange(Position second, double range)
        {
            return IsInRange(this, second, range);
        }

        public static bool IsInRange(Position first, Position second, double range)
        {
            int dx = (int)first.X - (int)second.X;
            int dy = (int)first.Y - (int)second.Y;
            return Math.Sqrt(dx * dx + dy * dy) <= range;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JSLib
{
    public class PlayerInfo
    {
        public int CharacterId;
        public string Name;
        public ushort Level;
        public Position SavedPosition;
        public int Str = 0;
        public int Dex = 0;
        public int Int = 0;
        public int Cons = 0;
        public int Wis = 0;
        public int Char = 0;
        public InventoryInfo Inventory = new InventoryInfo();
        public Dictionary<ushort, uint> SkillsLearnt = new Dictionary<ushort, uint>();
        public uint Experience = 0;
        public ushort ClassId = 0;
        public ushort RaceId = 0;
        public uint SkillPoints = 0;
    }
}

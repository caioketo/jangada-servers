﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;

namespace JSLib
{
    public class Connection
    {
        protected Socket socket;
        protected NetworkStream stream;
        protected NetworkMessage inMessage = new NetworkMessage(0);
        protected bool remove = false;
        protected ParsePacket parseFunction;
        protected OnError errorFunction;
        protected CanSend canSendFunction;
        protected List<NetworkMessage> buffer = new List<NetworkMessage>();

        public bool isWeb
        {
            get
            {
                return this is WebConnection;
            }
        }

        public Connection() { }

        public Connection(ParsePacket parse, OnError error, CanSend canSend)
        {
            parseFunction = parse;
            errorFunction = error;
            canSendFunction = canSend;
        }

        public void ListenerCallback(IAsyncResult ar)
        {
            TcpListener clientListener = (TcpListener)ar.AsyncState;
            socket = clientListener.EndAcceptSocket(ar);
            stream = new NetworkStream(socket);

            stream.BeginRead(inMessage.Buffer, 0, 4, new AsyncCallback(ClientReadCallback), null);
        }


        public void ClientReadCallback(IAsyncResult ar)
        {
            if (!EndRead(ar))
            {
                // disconnected
                if (errorFunction != null)
                {
                    errorFunction(this, null);
                }
                return;
            }

            if (inMessage.Length == 4)
            {
                //Don't know why, he recieve another message with 4 len
                //Prob. some echo.. (not always happens)
                stream.BeginRead(inMessage.Buffer, 0, 4, new AsyncCallback(ClientReadCallback), null);
                return;
            }

            byte type = inMessage.GetByte();

            Log.Add(DateTime.Now + " - MessageType: " + type);

            //Parse
            if (parseFunction != null)
            {
                parseFunction(type, inMessage, this);
            }

            if (!remove)
            {
                stream.BeginRead(inMessage.Buffer, 0, 4, new AsyncCallback(ClientReadCallback), null);
            }

        }


        private bool EndRead(IAsyncResult ar)
        {
            try
            {
                int read = stream.EndRead(ar);

                if (read == 0)
                {
                    // client disconnected
                    if (errorFunction != null)
                    {
                        errorFunction(this, null);
                    }
                    return false;
                }

                int size = (int)BitConverter.ToUInt32(inMessage.Buffer, 0) + 4;

                while (read < size)
                {
                    if (stream.CanRead)
                        read += stream.Read(inMessage.Buffer, read, size - read);
                }
                inMessage.Length = size;

                inMessage.Position = 0;

                inMessage.GetUInt32(); // total length

                return true;
            }
            catch (Exception ex)
            {
                //client disconnected
                if (errorFunction != null)
                {
                    errorFunction(this, ex);
                }
                return false;
            }
        }

        public void Send(NetworkMessage message)
        {
            if (isWeb)
            {
                ((WebConnection)this).Send(message, false);
            }
            else
            {
                Send(message, false);
            }
        }

        public void Send(NetworkMessage message, bool force)
        {
            if (isWeb)
            {
                ((WebConnection)this).Send(message, force);
                return;
            }
            if ((!canSendFunction(this)) && !force) 
            {
                buffer.Add(message);
                return;
            }
            if (!force && buffer.Count > 0)
            {
                NetworkMessage temp = message;
                foreach (NetworkMessage bufferMessage in buffer)
                {
                    temp = NetworkMessage.Concat(temp, bufferMessage);
                }
                message = temp;                
            }
            message.PrepareToSend();            
            Log.Add(DateTime.Now + " - Send: " + message.Buffer);
            try
            {
                string log = "";
                for (var i = 0; i < message.Length; i++)
                {
                    log += message.Buffer[i] + " ";
                }
                Log.Add(DateTime.Now + " - Mensagem enviada: " + log);
                stream.BeginWrite(message.Buffer, 0, message.Length, null, null);
                buffer.Clear();
            }
            catch (Exception ex)
            {
                // disconnected
                if (errorFunction != null)
                {
                    errorFunction(this, ex);
                }
            }
        }

        public void Close()
        {
            remove = true;
            stream.Close();
            socket.Close();
        }
    }
}

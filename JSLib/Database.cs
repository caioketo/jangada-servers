﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Npgsql;

namespace JSLib
{
    public class Database
    {
        private static string connectionStr = "Server=127.0.0.1;Port=5432;User Id=postgres;Password=vd001989;Database=jangada;";

        private static string validateAccount = "select id, password from \"Accounts\" where account = @account and status is null";
        private static string getCharacters = "select id, name from \"Characters\" where \"account_id\" = @accountId and status is null ";
        private static string getCharacterInfo = "select * from \"Characters\" where id = @id and status is null";
        private static string saveCharacterInfo = "update \"Characters\" set name = @name, level = @level, \"pos_x\" = @posX, \"pos_y\" = @posY, " +
            " \"pos_z\" = @posZ, \"stat_str\" = @statStr, \"stat_dex\" = @statDex, \"stat_int\" = @statInt, \"stat_cons\" = @statCons, " +
            " \"stat_wis\" = @statWis, \"stat_char\" = @statChar, experience = @experience where id = @id";

        private enum CharacterColumns : int
        {
            Id = 0,
            Name = 2,
            Level = 3,
            PosX = 4,
            PosY = 5,
            PosZ = 6,
            StatStr = 7,
            StatDex = 8,
            StatInt = 9,
            StatCons = 10,
            StatWis = 11,
            StatChar = 12,
            Experience = 15
        }

        public static int ValidateAcc(string account, string password)
        {
            using (NpgsqlConnection connection = new NpgsqlConnection(connectionStr))
            {
                connection.Open();
                using (NpgsqlCommand command = new NpgsqlCommand(validateAccount, connection))
                {
                    command.Parameters.AddWithValue("account", account.ToUpper());
                    using (NpgsqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            if (password.ToUpper() == reader.GetString(1).ToUpper())
                            {
                                return reader.GetInt32(0);
                            }
                        }
                    }
                }
            }

            return -1;
        }

        public static Dictionary<int, string> GetCharacterList(int accountId)
        {
            Dictionary<int, string> result = new Dictionary<int, string>();
            using (NpgsqlConnection connection = new NpgsqlConnection(connectionStr))
            {
                connection.Open();
                using (NpgsqlCommand command = new NpgsqlCommand(getCharacters, connection))
                {
                    command.Parameters.AddWithValue("accountId", accountId);
                    using (NpgsqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            result.Add(reader.GetInt32(0), reader.GetString(1));
                        }
                    }
                }
            }
            return result;
        }

        public static PlayerInfo GetPlayerInfo(uint characterId)
        {
            PlayerInfo info = new PlayerInfo();
            using (NpgsqlConnection connection = new NpgsqlConnection(connectionStr))
            {
                connection.Open();
                using (NpgsqlCommand command = new NpgsqlCommand(getCharacterInfo, connection))
                {
                    command.Parameters.AddWithValue("id", (int)characterId);
                    using (NpgsqlDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            info.Name = reader.GetString((int)CharacterColumns.Name);
                            info.Level = (ushort)reader.GetInt32((int)CharacterColumns.Level);
                            info.Experience = (uint)reader.GetInt64((int)CharacterColumns.Experience);
                            info.SavedPosition = new Position(reader.GetDouble((int)CharacterColumns.PosX), 
                                reader.GetDouble((int)CharacterColumns.PosY), reader.GetDouble((int)CharacterColumns.PosZ));
                            info.Str = reader.GetInt32((int)CharacterColumns.StatStr);
                            info.Dex = reader.GetInt32((int)CharacterColumns.StatDex);
                            info.Int = reader.GetInt32((int)CharacterColumns.StatInt);
                            info.Cons = reader.GetInt32((int)CharacterColumns.StatCons);
                            info.Wis = reader.GetInt32((int)CharacterColumns.StatWis);
                            info.Char = reader.GetInt32((int)CharacterColumns.StatChar);
                            info.Inventory.Items.Add(2, 23);
                            info.Inventory.Items.Add(12, 24);
                            info.SkillsLearnt.Add(1, 1);
                            info.SkillPoints = 3;
                        }
                    }
                }
            }
            return info;
        }

        public static void SavePlayerInfo(PlayerInfo player)
        {
            using (NpgsqlConnection connection = new NpgsqlConnection(connectionStr))
            {
                connection.Open();
                using (NpgsqlCommand command = new NpgsqlCommand(saveCharacterInfo, connection))
                {
                    command.Parameters.AddWithValue("id", player.CharacterId);
                    command.Parameters.AddWithValue("name", player.Name);
                    command.Parameters.AddWithValue("level", player.Level);
                    command.Parameters.AddWithValue("posX", player.SavedPosition.X);
                    command.Parameters.AddWithValue("posY", player.SavedPosition.Y);
                    command.Parameters.AddWithValue("posZ", player.SavedPosition.Z);
                    command.Parameters.AddWithValue("statStr", player.Str);
                    command.Parameters.AddWithValue("statDex", player.Dex);
                    command.Parameters.AddWithValue("statInt", player.Int);
                    command.Parameters.AddWithValue("statCons", player.Cons);
                    command.Parameters.AddWithValue("statWis", player.Wis);
                    command.Parameters.AddWithValue("statChar", player.Char);
                    command.Parameters.AddWithValue("experience", (long)player.Experience);
                    command.ExecuteNonQuery();
                }
            }
        }

    }
}

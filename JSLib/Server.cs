﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;

namespace JSLib
{
    public delegate void ParsePacket(byte type, NetworkMessage message, Connection connection);
    public delegate void OnError(Connection connection, Exception ex);
    public delegate bool CanSend(Connection connection);

    public class Server
    {
        private TcpListener listener;
        private Socket webListener;
        private int port;
        private int webPort;
        public List<Connection> connections = new List<Connection>();
        private ParsePacket parse;
        private OnError error = null;
        private CanSend canSend;

        public bool CanSendDefault(Connection connection)
        {
            return true;
        }

        public Server(int _port, ParsePacket _parse)
        {
            port = _port;
            parse = _parse;
            canSend = CanSendDefault;
            listener = new TcpListener(IPAddress.Any, port);
        }

        public Server(int _port, ParsePacket _parse, OnError _error)
        {
            port = _port;
            parse = _parse;
            error = _error;
            canSend = CanSendDefault;
            listener = new TcpListener(IPAddress.Any, port);
        }

        public Server(int _port, ParsePacket _parse, OnError _error, CanSend _canSend)
        {
            port = _port;
            parse = _parse;
            error = _error;
            if (_canSend == null)
            {
                canSend = CanSendDefault;
            }
            else
            {
                canSend = _canSend;
            }
            listener = new TcpListener(IPAddress.Any, port);
        }

        public Server(int _port, int _webPort, ParsePacket _parse, OnError _error, CanSend _canSend)
        {
            port = _port;
            parse = _parse;
            error = _error;
            if (_canSend == null)
            {
                canSend = CanSendDefault;
            }
            else
            {
                canSend = _canSend;
            }
            listener = new TcpListener(IPAddress.Any, port);
            webPort = _webPort;
            webListener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);
            webListener.Bind(new IPEndPoint(IPAddress.Any, webPort));
            webListener.Listen(1000);
        }

        public void Run()
        {
            listener.Start();
            listener.BeginAcceptSocket(new AsyncCallback(ListenerCallback), listener);
            if (webListener != null)
            {
                webListener.BeginAccept(new AsyncCallback(OnWebClientConnect), webListener);
            }
        }

        private void ListenerCallback(IAsyncResult ar)
        {
            Log.Add(DateTime.Now + " - Nova conexão");
            Connection connection = new Connection(parse, error, canSend);
            connection.ListenerCallback(ar);
            connections.Add(connection);
            listener.BeginAcceptSocket(new AsyncCallback(ListenerCallback), listener);
        }


        private void OnWebClientConnect(IAsyncResult ar)
        {
            Log.Add(DateTime.Now + " - Nova conexão");
            WebConnection connection = new WebConnection(parse, error, canSend);
            connection.ListenerCallback(ar);
            connections.Add(connection);
            webListener.BeginAccept(new AsyncCallback(OnWebClientConnect), webListener);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace JSLib
{
    public class NetworkMessage
    {
        #region Static Methods

        public static NetworkMessage Concat(NetworkMessage message1, NetworkMessage message2)
        {
            NetworkMessage concat = new NetworkMessage();
            Array.Copy(message1.buffer, 6, concat.buffer, 0, message1.length - 6);
            Array.Copy(message2.buffer, 6, concat.buffer, message1.length, message2.length - 6);
            concat.length = message1.length + message2.length - 6;
            return concat;
        }

        #endregion

        #region Instance Variables

        private byte[] buffer;
        private string JSONbuffer = "";
        private int position, length, bufferSize = 1105920;

        #endregion

        #region Properties

        public int Length
        {
            get { return length; }
            set { length = value; }
        }

        public string JSONBuffer
        {
            get { return JSONbuffer; }
        }

        public int Position
        {
            get { return position; }
            set { position = value; }
        }

        public byte[] Buffer
        {
            get { return buffer; }
            set { buffer = value; }
        }

        public int BufferSize
        {
            get { return bufferSize; }
        }

        public byte[] GetPacket()
        {
            byte[] t = new byte[length - 8];
            Array.Copy(buffer, 8, t, 0, length - 8);
            return t;
        }

        public byte[] ShortBuffer
        {
            get
            {
                byte[] t = new byte[length];
                Array.Copy(buffer, 0, t, 0, length);
                return t;
            }
        }

        public string JSONBuffer2
        {
            get
            {
                string json = "{";
                byte[] s = ShortBuffer;
                for (int i = 0; i < s.Length; i++)
                {
                    json += "\"" + i.ToString() + "\":" + s[i].ToString() + ",";
                }
                return json.Substring(0, json.Length - 1) + "}";
            }
        }

        #endregion

        #region Constructors

        public NetworkMessage()
        {
            Reset();
        }

        public NetworkMessage(int startingIndex)
        {
            Reset(startingIndex);
        }

        public void Reset(int startingIndex)
        {
            buffer = new byte[bufferSize];
            length = startingIndex;
            position = startingIndex;
        }

        public void Reset()
        {
            Reset(6);
        }

        #endregion

        #region Get

        public byte GetByte()
        {
            if (position + 1 > length)
                throw new IndexOutOfRangeException("NetworkMessage GetByte() out of range.");

            return buffer[position++];
        }

        public byte[] GetBytes(int count)
        {
            if (position + count > length)
                throw new IndexOutOfRangeException("NetworkMessage GetBytes() out of range.");

            byte[] t = new byte[count];
            Array.Copy(buffer, position, t, 0, count);
            position += count;
            return t;
        }

        public string GetString()
        {
            int len = (int)GetUInt16();
            string t = System.Text.ASCIIEncoding.Default.GetString(buffer, position, len);
            position += len;
            return t;
        }

        public ushort GetUInt16()
        {
            return BitConverter.ToUInt16(GetBytes(2), 0);
        }

        public uint GetUInt32()
        {
            return BitConverter.ToUInt32(GetBytes(4), 0);
        }

        public Guid GetGuid()
        {
            return new Guid(GetBytes(16));
        }

        private ushort GetPacketHeader()
        {
            return BitConverter.ToUInt16(buffer, 0);
        }

        public double GetDouble()
        {
            return (GetUInt32() / 10);
        }

        public Position GetPosition()
        {
            double x = GetDouble();
            double y = GetDouble();
            double z = GetDouble();
            return new Position(x, y, z);
        }

        #endregion

        #region Add

        public void AddByte(byte value)
        {
            if (1 + length > bufferSize)
                throw new Exception("NetworkMessage buffer is full.");

            AddBytes(new byte[] { value });
        }

        public void AddBytes(byte[] value)
        {
            if (value.Length + length > bufferSize)
                throw new Exception("NetworkMessage buffer is full.");

            Array.Copy(value, 0, buffer, position, value.Length);
            for (int i = 0; i < value.Length; i++)
            {
                JSONbuffer += "\"" + position.ToString() + "\":" + value[i].ToString() + ",";
                position++;
            }
            //position += value.Length;

            if (position > length)
                length = position;
        }

        public void AddString(string value)
        {
            AddUInt16((ushort)value.Length);
            AddBytes(System.Text.ASCIIEncoding.Default.GetBytes(value));
        }

        public void AddUInt16(ushort value)
        {
            AddBytes(BitConverter.GetBytes(value));
        }

        public void AddUInt32(uint value)
        {
            AddBytes(BitConverter.GetBytes(value));
        }

        public void AddGuid(Guid value)
        {
            AddBytes(value.ToByteArray());
        }

        public void AddPaddingBytes(int count)
        {
            position += count;

            if (position > length)
                length = position;
        }


        public void AddDouble(double value)
        {
            uint iValue = (uint)(value * 10);
            AddUInt32(iValue);
        }

        public void AddPosition(Position pos)
        {
            AddDouble(pos.X);
            AddDouble(pos.Y);
            AddDouble(pos.Z);
        }

        #endregion

        #region Peek

        public byte PeekByte()
        {
            return buffer[position];
        }

        public byte[] PeekBytes(int count)
        {
            byte[] t = new byte[count];
            Array.Copy(buffer, position, t, 0, count);
            return t;
        }

        public ushort PeekUInt16()
        {
            return BitConverter.ToUInt16(PeekBytes(2), 0);
        }

        public uint PeekUInt32()
        {
            return BitConverter.ToUInt32(PeekBytes(4), 0);
        }

        public string PeekString()
        {
            int len = (int)PeekUInt16();
            return System.Text.ASCIIEncoding.ASCII.GetString(PeekBytes(len + 2), 2, len);
        }

        #endregion

        #region Replace

        public void ReplaceBytes(int index, byte[] value)
        {
            if (length - index >= value.Length)
                Array.Copy(value, 0, buffer, index, value.Length);
        }

        #endregion

        #region Skip

        public void SkipBytes(int count)
        {
            if (position + count > length)
                throw new IndexOutOfRangeException("NetworkMessage SkipBytes() out of range.");
            position += count;
        }

        #endregion

        #region Prepare

        private void InsertIdentifier()
        {
            byte[] id = { 0x01, 0x02 };
            Array.Copy(id, 0, buffer, 0, 2);
        }

        private void InsertTotalLength()
        {
            try
            {
                byte[] len = new byte[4];
                len = BitConverter.GetBytes((int)(length - 6));
                string temp = JSONbuffer;
                JSONbuffer = "{\"0\":1, \"1\":2,";
                for (int i = 0; i < len.Length; i++)
                {
                    JSONbuffer += "\"" + (i+2).ToString() + "\":" + len[i].ToString() + ",";
                }
                JSONbuffer += temp.Substring(0, temp.Length - 1) + "}";
                Array.Copy(len, 0, buffer, 2, 4);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool PrepareToSendWithoutEncryption()
        {
            InsertIdentifier();
            InsertTotalLength();

            return true;
        }

        public bool PrepareToSend()
        {
            InsertIdentifier();
            InsertTotalLength();

            return true;
        }

        public bool PrepareToRead()
        {
            position = 4;
            return true;
        }

        #endregion
    }
}

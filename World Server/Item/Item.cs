﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World_Server.Item
{
    public class Item : Thing
    {
        public ushort Id;

        private Item()
        {
        }

        protected Item(ushort id)
        {
            Id = id;
        }

        public static Item Create(ushort id)
        {
            return new Item(id);
        }

        public override ushort GetThingId()
        {
            return Id;
        }
    }
}

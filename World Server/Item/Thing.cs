﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World_Server.Item
{
    public abstract class Thing
    {
        public abstract ushort GetThingId();
    }
}

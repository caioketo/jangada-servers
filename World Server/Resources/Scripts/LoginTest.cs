﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using World_Server;
using World_Server.Scripting;
using World_Server.Creatures;

public class LoginTest : IScript
{
    Game game;

    public bool Start(Game game)
    {
        this.game = game;

        game.AfterLogin += AfterLogin;

        return true;
    }

    public bool Stop()
    {
        game.AfterLogin -= AfterLogin;
        return true;
    }


    public void AfterLogin(Player player)
    {
    }
}

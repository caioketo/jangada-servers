﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using World_Server;
using World_Server.Scripting;
using World_Server.Creatures;
using World_Server.Creatures.Specs;
using World_Server.Utils;

public class SecondSkill : ISkill
{
    Skill skill;
    Player caster;

    public void onInit(Skill skill, Player caster)
    {
        //onInit = Passives uses it
        this.skill = skill;
        this.caster = caster;
    }

    public void onCast()
    {
        //onCast = Castables
    }

    public void onActivate()
    {
        //onActivate = Activables
    }

    public void onDesactivate()
    {
        //onDesactivate = Activables 
    }

    public void onEnd()
    {
        //onEnd
    }

    public SkillInfo getInfo()
    {
        SkillInfo info = new SkillInfo();

        info.Description = "Second Skill";
        info.Id = 2;
        info.LevelRequired = 0;
        info.Cooldown = 10;
        info.Type = SkillType.Damage;
        info.Class = (ushort)ClassesIds.Warrior;
        info.PositionInTree = 1;
        info.effect = Effect.Blood;
        info.Targetable = true;
        info.AreaEffect = new string[][]
        {
            new string[] {"0", "1", "0"},
            new string[] {"1", "X", "1"},
            new string[] {"0", "1", "0"}
        };
        return info;
    }


    public int getDamage()
    {
        return 100 + ((int)skill.Level * 2);
    }
}

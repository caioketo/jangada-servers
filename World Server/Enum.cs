﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World_Server
{
    public enum ClientType
    {
        Web
    }

    public enum TextMessageType : byte
    {
        Default = 1
    }

    public enum StatType : byte
    {
        STR = 1,
        DEX = 2,
        CONS = 3,
        INT = 4,
        WIS = 5,
        CHAR = 6,
        ALL
    }

    public enum Effect : byte
    {
        Login = 1,
        Blood = 2,
        Health = 3
    }

    public enum TextColor : byte
    {
        Red = 1,
        Green = 2
    }

    public enum SlotType : byte
    {
        None = 0,
        Right_Earing = 1,
        Head = 2,
        Left_Earing = 3,
        Right_Shoulder = 4,
        Neck = 5,
        Left_Shoulder = 6,
        Right_Hand = 7,
        Armor = 8,
        Left_Hand = 9,
        Leg = 10,
        Foot = 11,
        Container1 = 12,
        Container2 = 13,
        Container3 = 14,
        Container4 = 15,
        First = Right_Earing,
        Last = Container4
    }

    public enum LootOption : int
    {
        Need = 0,
        Greed = 1,
        Pass = 2
    }

    public enum ItemGroup
    {
        None = 0,
        Ground,
        Container,
        Equipment,
        Weapon
    }

    public enum ContainerIndex : byte
    {
        Container1 = 1,
        Container2 = 2,
        Container3 = 3,
        Container4 = 4
    }

    public enum SkillType
    {
        Activable,
        Castable,
        Damage,
        Passive
    }

    public enum ClassesIds : ushort
    {
        Warrior = 0,
        Archer = 1
    }

    public enum RacesIds : ushort
    {
        Human = 0
    }

    public enum AttributeType
    {
        None,
        AttackSpeed,
        AttackDamage
    }

    public enum TargetType : byte
    {
        Attack = 0,
        Follow = 1
    }
}

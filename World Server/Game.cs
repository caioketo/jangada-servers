﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using World_Server.Creatures;
using JSLib;
using World_Server.Packets;
using World_Server.Packets.Server;
using World_Server.Utils;
using World_Server.Scripting;
using World_Server.Items;
using World_Server.Packets.Client;

namespace World_Server
{
    public class Game
    {
        System.Threading.Timer TimerCreatures;
        System.Threading.Timer TimerRespawns;

        private void UpdateCreatures(object state)
        {
            //Update every creature...
            foreach (Creature creature in Creatures)
            {
                creature.Update();
            }

            foreach (Player player in Players)
            {
                player.Update();
            }
        }

        private void UpdateRespawns(object state)
        {
            //Update respawns...
            RespawnHandler.Instance.Update();
        }

        public void Run()
        {
            TimerCreatures = new System.Threading.Timer(UpdateCreatures, null, 0, 500);
            TimerRespawns = new System.Threading.Timer(UpdateRespawns, null, 0, 1500);
        }

        public WorldServer Server;
        private static Game instance;
        public static Game Instance
        {
            get
            {
                return instance;
            }
        }


        #region Events
        public BeforeLoginHandler BeforeLogin;
        public AfterLoginHandler AfterLogin;
        #endregion


        public Game(WorldServer server)
        {
            instance = this;
            Players = new List<Player>();
            Creatures = new List<Creature>();
            Items = new List<Item>();
            Loots = new List<Loot>();
            Server = server;
        }

        private List<Player> Players;
        private List<Item> Items;
        private List<Creature> Creatures;
        private List<Loot> Loots;

        private Loot GetLoot(int id)
        {
            return Loots.Where(l => l.Id == id).FirstOrDefault();
        }


        private int GetNextLootId()
        {
            if (Loots.Count <= 0)
            {
                return 1;
            }
            Loot lastLoot = Loots.OrderBy(l => l.Id).Last();
            return lastLoot.Id + 1;
        }

        public void AddCreature(Creature creature)
        {
            if (!Creatures.Contains(creature))
            {
                Guid newGuid = Guid.NewGuid();
                while (newGuid.Equals(Guid.Empty))
                {
                    newGuid = Guid.NewGuid();
                }
                creature.guid = newGuid;
                Creatures.Add(creature);
            }
        }

        public void AddItem(Item item)
        {
            if (!Items.Contains(item))
            {
                Guid newGuid = Guid.NewGuid();
                while (newGuid.Equals(Guid.Empty))
                {
                    newGuid = Guid.NewGuid();
                }
                item.guid = newGuid;
                Items.Add(item);
            }
        }

        public Creature GetCreatureByGuid(Guid guid)
        {
            return Creatures.Where(creature => creature.guid == guid).FirstOrDefault();
        }

        public Item GetItemByGuid(Guid guid)
        {
            return Items.Where(item => item.guid == guid).FirstOrDefault();
        }

        public IEnumerable<Player> GetSpectatorPlayers(Position position)
        {
            return Players.Where(player => player.tile.position.CanSee(position));
        }

        public void ClearTarget(Creature target)
        {
            foreach (Creature creature in Creatures.Where(c => c.Target == target))
            {
                creature.Target = null;
            }

            foreach (Player player in Players.Where(p => p.Target == target))
            {
                player.Target = null;
            }
        }

        public void AddPlayer(Player player)
        {
            if (!Players.Contains(player))
            {
                Players.Add(player);
            }
        }

        public Player GetPlayer(Connection connection)
        {
            foreach (Player player in Players)
            {
                if (player.connection == connection)
                {
                    return player;
                }
            }

            return null;
        }

        public void ProcessLogin(Connection connection, LoginWorldPacket packet)
        {
            PlayerInfo info = Database.GetPlayerInfo(packet.CharacterId);
            Player player = new Player(info);
            player.connection = connection;
            player.Client = packet.Client;
            PlayerLogin(player);
        }

        public void PlayerLogout(Player player)
        {
            if (player == null) return;
            Database.SavePlayerInfo(player.GetInfo());
            player.connection.Close();
            player.tile.Creatures.Remove(player);
            Players.Remove(player);
        }

        private void PlayerLogin(Player player)
        {
            AddPlayer(player);
            Server.SendInitialPacket(player);
            player.tile.Creatures.Add(player);

            foreach (Player p in Players)
            {
                if (p != player)
                {
                    if (p.tile.position.CanSee(player.tile.position))
                    {
                        Server.SendCreatureAppear(player, p.connection);
                    }
                }
            }

            if (AfterLogin != null)
            {
                foreach (Delegate del in AfterLogin.GetInvocationList())
                {
                    AfterLoginHandler subscriber = (AfterLoginHandler)del;
                    subscriber(player);
                }
            }
        }

        public void CreatureAddHealth(Creature creature, uint health)
        {
            Position tPos = creature.tile.position;
            creature.Health += health;

            foreach (Player player in GetSpectatorPlayers(tPos))
            {
                WNetworkMessage outMessage = new WNetworkMessage();
                EffectPacket.Add(outMessage, tPos, Effect.Health);
                TileTextPacket.Add(outMessage, tPos, health.ToString(), TextColor.Green);
                if (creature.tile != null)
                {
                    long hpPct = ((creature.Health * 100) / creature.MaxHealth);
                    CreatureUpdatePacket.Add(outMessage, creature.tile.position, creature.tile.GetStackPosition(creature), (byte)(hpPct));
                }
                if (creature == player)
                {
                    PlayerStatusPacket.Add(outMessage, player);
                }
                player.connection.Send(outMessage);
            }
        }

        public void CreatureApplyDamage(Creature creature, Creature target, int damage)
        {
            Position tPos = target.tile.position;
            int dmg = target.ApplyDamage(damage, creature);

            foreach (Player player in GetSpectatorPlayers(tPos))
            {
                WNetworkMessage outMessage = new WNetworkMessage();
                EffectPacket.Add(outMessage, tPos, Effect.Blood);
                TileTextPacket.Add(outMessage, tPos, dmg.ToString(), TextColor.Red);
                if (target.tile != null)
                {
                    long hpPct = ((target.Health * 100) / target.MaxHealth);
                    CreatureUpdatePacket.Add(outMessage, target.tile.position, target.tile.GetStackPosition(target), (byte)(hpPct));
                }
                if (target == player)
                {
                    PlayerStatusPacket.Add(outMessage, player);
                }
                player.connection.Send(outMessage);
            }
        }

        public void CreatureAttack(Creature creature, Creature target)
        {
            CreatureApplyDamage(creature, target, creature.GetDamage());
        }

        public void CreatureWalk(Creature creature, Direction direction)
        {
            creature.LastStepCost = 1;

            CreatureMove(creature, creature.tile.position.Offset(direction));
        }


        public void HandleTouch(Player player, Position touchedPos)
        {
            Tile touchedTile = Map.Instance.GetTile(touchedPos);
            if (touchedTile == null) return;
            Creature c = touchedTile.GetTopCreature();
            if (c == null) return;
            if (c == player) return;

            player.Target = c;
            player.TargetType = TargetType.Attack;
            //Send message TargetSelected
            WNetworkMessage outMessage = new WNetworkMessage();
            SetTargetPacket.Add(outMessage, c.tile.position, c.tile.GetStackPosition(c), player.TargetType);
            player.connection.Send(outMessage);
        }

        public void CreatureDie(Creature creature)
        {
            ClearTarget(creature);
            Tile cTile = creature.tile;
            byte stackPos = cTile.GetStackPosition(creature);
            cTile.Creatures.Remove(creature);
            Creatures.Remove(creature);
            creature.tile = null;
            Item body = Item.Create(creature.Info.BodyId);
            cTile.AddItem(body);
            List<Player> Attackers = creature.GetAttackers();

            foreach (Player player in GetSpectatorPlayers(cTile.position))
            {
                WNetworkMessage outMessage = new WNetworkMessage();
                TileRemoveThingPacket.Add(outMessage, cTile.position, stackPos);
                TileAddItemPacket.Add(outMessage, cTile.position, body);
                player.connection.Send(outMessage);
            }

            List<Item> drops = creature.Info.loot.GetDrop();
            if (drops.Count > 0)
            {
                Loot loot = new Loot(drops, Attackers, GetNextLootId());
                Loots.Add(loot);
                foreach (Player player in Attackers)
                {
                    WNetworkMessage outMessage = new WNetworkMessage();
                    LootPacket.Add(outMessage, loot);
                    player.connection.Send(outMessage);
                }
            }

            if (Attackers.Count > 0)
            {
                uint exp = creature.Info.Experience;
                exp = Convert.ToUInt32(exp / Attackers.Count);

                foreach (Player player in Attackers)
                {
                    player.AddExp(exp);
                }
            }
        }

        public void CreatureMove(Creature creature, Position toPosition)
        {
            Tile fromTile = creature.tile;
            Position fromPos = fromTile.position;
            byte fromStackPos = fromTile.GetStackPosition(creature);
            Tile toTile = Map.Instance.GetTile(toPosition);

            if (toTile != null)
            {
                fromTile.Creatures.Remove(creature);
                toTile.Creatures.Add(creature);
                creature.tile = toTile;

                if (fromPos.Y > toPosition.Y)
                    creature.direction = Direction.North;
                else if (fromPos.Y < toPosition.Y)
                    creature.direction = Direction.South;
                if (fromPos.X < toPosition.X)
                    creature.direction = Direction.East;
                else if (fromPos.X > toPosition.X)
                    creature.direction = Direction.West;

                foreach (var player in Players)
                {
                    WNetworkMessage outMessage = new WNetworkMessage();
                    if (player == creature)
                    {
                        CreatureMovePacket.Add(outMessage, fromPos, fromStackPos, toPosition, creature.GetStepDuration());
                        MapSlicePacket.Add(player.connection, outMessage, fromPos, toPosition);
                    }
                    else if (player.tile.position.CanSee(fromPos) && player.tile.position.CanSee(toPosition))
                    {
                        CreatureMovePacket.Add(outMessage, fromPos, fromStackPos, toPosition, creature.GetStepDuration());
                    }
                    else if (player.tile.position.CanSee(fromPos))
                    {
                        TileRemoveThingPacket.Add(outMessage, fromPos, fromStackPos);
                    }
                    else if (player.tile.position.CanSee(toPosition))
                    {
                        TileAddCreaturePacket.Add(outMessage, toPosition, creature);
                    }
                    player.connection.Send(outMessage);
                }
            }

        }

        public void HandleMessage(Player player, string message, byte type)
        {
            WNetworkMessage outMessage = new WNetworkMessage();
            CreatureSpeakPacket.Add(outMessage, message, player.tile.position, type);
            foreach (var spec in Players)
            {
                if (spec.tile.position.CanSee(player.tile.position))
                {
                    spec.connection.Send(outMessage);
                }
            }
        }

        public void LootItemChoice(Player player, int lootId, int lootItemId, int option)
        {
            Loot loot = GetLoot(lootId);
            if (loot == null) return;

            loot.Choose(player, lootItemId, option);

            if (loot.CheckItem(lootItemId))
            {
                Item lootItem = loot.GetLootItem(lootItemId);
                Player winner = loot.GetWinner(lootItemId);
                
                if (winner != null)
                {
                    //add Item no inventary do player
                    byte containerIndex = 0x00;
                    if (winner.Inventory.GetItemInSlot(SlotType.Container1) != null && !(((Container)winner.Inventory.GetItemInSlot(SlotType.Container1)).IsFull()))
                    {
                        Container cont = (Container)winner.Inventory.GetItemInSlot(SlotType.Container1);
                        cont.AddItem(lootItem);
                        containerIndex = (byte)ContainerIndex.Container1;
                    }
                    else if (winner.Inventory.GetItemInSlot(SlotType.Container2) != null && !(((Container)winner.Inventory.GetItemInSlot(SlotType.Container2)).IsFull()))
                    {
                        Container cont = (Container)winner.Inventory.GetItemInSlot(SlotType.Container2);
                        cont.AddItem(lootItem);
                        containerIndex = (byte)ContainerIndex.Container2;
                    }
                    else if (winner.Inventory.GetItemInSlot(SlotType.Container3) != null && !(((Container)winner.Inventory.GetItemInSlot(SlotType.Container3)).IsFull()))
                    {
                        Container cont = (Container)winner.Inventory.GetItemInSlot(SlotType.Container3);
                        cont.AddItem(lootItem);
                        containerIndex = (byte)ContainerIndex.Container3;
                    }
                    else if (winner.Inventory.GetItemInSlot(SlotType.Container4) != null && !(((Container)winner.Inventory.GetItemInSlot(SlotType.Container4)).IsFull()))
                    {
                        Container cont = (Container)winner.Inventory.GetItemInSlot(SlotType.Container4);
                        cont.AddItem(lootItem);
                        containerIndex = (byte)ContainerIndex.Container4;
                    }

                    if (containerIndex != 0x00)
                    {
                        WNetworkMessage outMessage = new WNetworkMessage();
                        ContainerAddItemPacket.Add(outMessage, containerIndex, lootItem);
                        winner.connection.Send(outMessage);
                    }
                }

                if (loot.Items.Count == 0)
                {
                    Loots.Remove(loot);
                }
            }
        }

        public void OnItemUse(ContainerIndex index, byte containerPos, Player player)
        {
            Item item = null;
            Container container = null;
            switch (index)
            {
                case ContainerIndex.Container1:
                    container = (Container)player.Inventory.GetItemInSlot(SlotType.Container1);
                    item = container.GetItem(containerPos);
                    break;
                case ContainerIndex.Container2:
                    break;
                case ContainerIndex.Container3:
                    break;
                case ContainerIndex.Container4:
                    break;
                default:
                    break;
            }

            if (item == null) return;
            if (item.Info.Group == ItemGroup.Equipment)
            {
                WNetworkMessage outMessage = new WNetworkMessage();
                //ContainerRemoveItem
                ContainerRemoveItemPacket.Add(outMessage, (byte)ContainerIndex.Container1, containerPos);
                container.RemoveItem(containerPos);
                if (player.Inventory.GetItemInSlot(item.Info.slot) != null)
                {
                    //InventoryRemoveItem
                    ContainerAddItemPacket.Add(outMessage, (byte)ContainerIndex.Container1, player.Inventory.GetItemInSlot(item.Info.slot));
                    container.AddItem(player.Inventory.GetItemInSlot(item.Info.slot));
                    InventoryRemoveSlotItem.Add(outMessage, item.Info.slot);
                    player.Inventory.SetItemInSlot(null, item.Info.slot);                    
                }
                InventorySetSlotPacket.Add(outMessage, item, item.Info.slot);
                player.Inventory.SetItemInSlot(item, item.Info.slot);
                player.connection.Send(outMessage);
            }
        }

        public void ItemDecay(Item item, Item i)
        {
            Tile itemTile = item.tile;
            byte stackPos = itemTile.GetStackPosition(item);
            itemTile.RemoveItem(item);
            if (i != null)
            {
                itemTile.AddItem(i);
            }

            foreach (Player player in GetSpectatorPlayers(itemTile.position))
            {
                WNetworkMessage outMessage = new WNetworkMessage();
                TileRemoveThingPacket.Add(outMessage, itemTile.position, stackPos);
                if (i != null)
                {
                    TileAddItemPacket.Add(outMessage, itemTile.position, i);
                }
                player.connection.Send(outMessage);
            }
        }

        internal void InventoryRemoveItem(byte slot, Connection connection)
        {
            Player player = GetPlayer(connection);
            Item item = player.Inventory.GetItemInSlot((SlotType)slot);
            if (item == null) return;

            Container container = null;

            if (player.Inventory.GetItemInSlot(SlotType.Container1) != null && !((Container)player.Inventory.GetItemInSlot(SlotType.Container1)).IsFull())
            {
                container = (Container)player.Inventory.GetItemInSlot(SlotType.Container1);
            }
            else if (player.Inventory.GetItemInSlot(SlotType.Container2) != null && !((Container)player.Inventory.GetItemInSlot(SlotType.Container2)).IsFull())
            {
                container = (Container)player.Inventory.GetItemInSlot(SlotType.Container2);
            }
            else if (player.Inventory.GetItemInSlot(SlotType.Container3) != null && !((Container)player.Inventory.GetItemInSlot(SlotType.Container3)).IsFull())
            {
                container = (Container)player.Inventory.GetItemInSlot(SlotType.Container3);
            }
            else if (player.Inventory.GetItemInSlot(SlotType.Container4) != null && !((Container)player.Inventory.GetItemInSlot(SlotType.Container4)).IsFull())
            {
                container = (Container)player.Inventory.GetItemInSlot(SlotType.Container4);
            }

            if (container == null) return;

            WNetworkMessage outMessage = new WNetworkMessage();
            ContainerAddItemPacket.Add(outMessage, (byte)ContainerIndex.Container1, player.Inventory.GetItemInSlot((SlotType)slot));
            container.AddItem(player.Inventory.GetItemInSlot((SlotType)slot));
            InventoryRemoveSlotItem.Add(outMessage, (SlotType)slot);
            player.Inventory.SetItemInSlot(null, (SlotType)slot);
            connection.Send(outMessage);
        }
    }
}

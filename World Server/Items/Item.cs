﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace World_Server.Items
{
    public class Item : Thing
    {
        public ushort Id;
        public Guid guid { get; set; }
        public Tile tile;
        public Timer decayTimmer;

        private void Decay(object source, ElapsedEventArgs e)
        {
            this.decayTimmer.Dispose();
            Item i = null;
            if (this.Info.DecayTo > 0)
            {
                i = Item.Create(this.Info.DecayTo);
            }
            Game.Instance.ItemDecay(this, i);
        }

        private Item()
        {
        }

        protected Item(ushort id)
        {
            Id = id;
            Game.Instance.AddItem(this);
        }

        public static Item Create(ushort id)
        {
            ItemInfo info = ItemInfo.GetItemInfo(id);
            switch (info.Group)
            {
                case ItemGroup.Container:
                    return new Container(id);
                default:
                    Item i = new Item(id);
                    if (i.Info.DecayTime > 0)
                    {
                        i.decayTimmer = new Timer(i.Info.DecayTime);
                        i.decayTimmer.AutoReset = true;
                        i.decayTimmer.Elapsed += new ElapsedEventHandler(i.Decay);
                        i.decayTimmer.Start();
                    }
                    return i;
            }
        }

        public override ushort GetThingId()
        {
            return Id;
        }

        public ItemInfo Info
        {
            get
            {
                return ItemInfo.GetItemInfo(Id);
            }
        }

    }
}

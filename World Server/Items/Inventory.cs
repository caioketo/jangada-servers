﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World_Server.Items
{
    public class Inventory
    {
        Item[] slotItems = new Item[(int)SlotType.Last];

        public Item GetItemInSlot(SlotType fromSlot)
        {
            return slotItems[(int)fromSlot - 1];
        }

        public void SetItemInSlot(Item item, SlotType toSlot)
        {
            slotItems[(int)toSlot - 1] = item;
        }

        public IEnumerable<KeyValuePair<SlotType, Item>> GetSlotItems()
        {
            for (SlotType slot = SlotType.First; slot <= SlotType.Last; ++slot)
            {
                Item item = GetItemInSlot(slot);
                if (item != null)
                {
                    yield return new KeyValuePair<SlotType, Item>(slot, item);
                }
            }
        }
    }
}

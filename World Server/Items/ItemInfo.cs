﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World_Server.Items
{
    public class ItemInfo
    {
        public ushort SpriteId;
        public ushort Speed;
        public string Description;
        public ushort DecayTo = 0;
        public long DecayTime = 0;
        public byte Volume;
        public ItemGroup Group;
        public SlotType slot;

        private static Dictionary<ushort, ItemInfo> itemInfoDictionary = new Dictionary<ushort, ItemInfo>();

        public static ItemInfo GetItemInfo(ushort id)
        {
            return itemInfoDictionary[id];
        }

        public static Dictionary<ushort, ItemInfo> GetDictionary()
        {
            return itemInfoDictionary;
        
        }

        public static void SetDictionary(Dictionary<ushort, ItemInfo> dic)
        {
            itemInfoDictionary = dic;
        }
    }
}

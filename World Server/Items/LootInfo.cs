﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World_Server.Items
{
    public class LootInfo
    {
        public List<ushort> Ids = new List<ushort>();
        public List<int> Chances = new List<int>();

        public List<Item> GetDrop()
        {
            List<Item> drop = new List<Item>();

            for (int i = 0; i < Ids.Count; i++)
            {
                Random rand = new Random();
                double r = rand.NextDouble();
                if ((r * 100) <= Chances[i])
                {
                    drop.Add(Item.Create(Ids[i]));
                }
            }


            return drop;
        }
    }
}

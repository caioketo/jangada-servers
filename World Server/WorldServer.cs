﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;
using World_Server.Packets;
using World_Server.Packets.Client;
using World_Server.Packets.Server;
using World_Server.Creatures;
using World_Server.Utils;
using World_Server.Scripting;
using World_Server.Creatures.Specs;

namespace World_Server
{
    public class WorldServer
    {
        static void Main(string[] args)
        {
            new WorldServer().Run();
        }

        Server server;

        void Run()
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(AllHandler);
            new Game(this);
            Loader.LoadLevels();
            Loader.LoadCreatures();
            Loader.LoadItems();
            Loader.LoadMap();
            Loader.LoadRespawns();
            string scriptErrors = ScriptManager.LoadAllScripts();
            Console.WriteLine(scriptErrors);
            Loader.LoadClasses();
            Loader.LoadRaces();
            server = new Server(7171, 7373, ParsePacket, OnError, CanSend);
            server.Run();
            Game.Instance.Run();
            while (true)
            {
                string line = Console.ReadLine();

                if (line == "exit")
                {
                    break;
                }
            }
        }

        static void AllHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            Console.WriteLine("AllHandler caught : " + e.Message);
        }

        public void OnError(Connection connection, Exception ex)
        {
            OnLogout(connection);
            connection.Close();
            if (ex != null)
            {
                Console.WriteLine("OnError: " + ex.Message);
            }
        }

        public bool CanSend(Connection connection)
        {
            return Game.Instance.GetPlayer(connection).isLogged;
        }


        public void OnLogout(Connection connection)
        {
            Game.Instance.PlayerLogout(Game.Instance.GetPlayer(connection));
        }

        public void ParsePacket(byte type, NetworkMessage message, Connection connection)
        {
            WNetworkMessage outMessage = new WNetworkMessage();

            switch ((ClientTypePacket)type)
            {
                case ClientTypePacket.LoginWorld:
                    LoginWorldPacket loginPacket = LoginWorldPacket.Parse(message);
                    Game.Instance.ProcessLogin(connection, loginPacket);
                    break;
                case ClientTypePacket.MoveEast:
                    ParseWalk(Direction.East, connection);
                    break;
                case ClientTypePacket.MoveWest:
                    ParseWalk(Direction.West, connection);
                    break;
                case ClientTypePacket.MoveNorth:
                    ParseWalk(Direction.North, connection);
                    break;
                case ClientTypePacket.MoveSouth:
                    ParseWalk(Direction.South, connection);
                    break;
                case ClientTypePacket.TouchedPos:
                    TouchedPosPacket packet = TouchedPosPacket.Parse(message);
                    ParseTouch(packet.touchedPos, connection);
                    break;
                case ClientTypePacket.Message:
                    MessagePacket mPacket = MessagePacket.Parse(message);
                    ParseMessage(connection, mPacket);
                    break;
                case ClientTypePacket.AddStatus:
                    AddStatusPacket aSPacket = AddStatusPacket.Parse(message);
                    ParseAddStatus(connection, aSPacket);
                    break;
                case ClientTypePacket.LootOption:
                    LootOptionPacket.Parse(message, connection);
                    break;
                case ClientTypePacket.OnItemUseInContainer:
                    OnItemUseInContainerPacket iPacket = OnItemUseInContainerPacket.Parse(message);
                    Game.Instance.OnItemUse(iPacket.containerIndex, iPacket.containerPos, Game.Instance.GetPlayer(connection));
                    break;
                case ClientTypePacket.InventoryRemoveItem:
                    InventoryRemoveItemPacket iRemovePacket = InventoryRemoveItemPacket.Parse(message);
                    Game.Instance.InventoryRemoveItem(iRemovePacket.Slot, connection);
                    break;
                case ClientTypePacket.SkillUse:
                    OnSkillUsePacket skillPacket = OnSkillUsePacket.Parse(message);
                    ParseSkillUse(connection, skillPacket);
                    break;
                case ClientTypePacket.LearnSkill:
                    LearnSkillPacket learnPacket = LearnSkillPacket.Parse(message);
                    ParseLearnSkill(connection, learnPacket.skillId);
                    break;
                case ClientTypePacket.CreatureAttack:
                    CreatureAttackPacket cAttackPacket = CreatureAttackPacket.Parse(message);
                    ParseCreatureAttack(connection, cAttackPacket);
                    break;
                case ClientTypePacket.CreatureFollow:
                    CreatureFollowPacket cFollowPacket = CreatureFollowPacket.Parse(message);
                    ParseCreatureFollow(connection, cFollowPacket);
                    break;
                case ClientTypePacket.OnItemUse:
                    OnItemUsePacket itemUsePacket = OnItemUsePacket.Parse(message);
                    break;
                case ClientTypePacket.Logged:
                    Game.Instance.GetPlayer(connection).isLogged = true;
                    break;
            }

        }

        private void ParseCreatureAttack(Connection connection, CreatureAttackPacket packet)
        {
            Player player = Game.Instance.GetPlayer(connection);
            Creature c = Game.Instance.GetCreatureByGuid(packet.guid);
            player.Target = c;
            player.TargetType = TargetType.Attack;
            //Send message TargetSelected
            WNetworkMessage outMessage = new WNetworkMessage();
            SetTargetPacket.Add(outMessage, c.tile.position, c.tile.GetStackPosition(c), player.TargetType);
            player.connection.Send(outMessage);
        }

        private void ParseCreatureFollow(Connection connection, CreatureFollowPacket packet)
        {
            Player player = Game.Instance.GetPlayer(connection);
            Creature c = Game.Instance.GetCreatureByGuid(packet.guid);
            player.Target = c;
            player.TargetType = TargetType.Follow;
            //Send message TargetSelected
            WNetworkMessage outMessage = new WNetworkMessage();
            SetTargetPacket.Add(outMessage, c.tile.position, c.tile.GetStackPosition(c), player.TargetType);
            player.connection.Send(outMessage);
            player.DoFollow();
        }

        private void ParseLearnSkill(Connection connection, uint id)
        {
            Player player = Game.Instance.GetPlayer(connection);
            if (player.GetSkill(id) == null)
            {
                SkillInfo learn = SkillInfo.GetSkillInfo((ushort)id);
                if (learn.LevelRequired > player.Level)
                {
                    return;
                }

                for (int i = 0; i < learn.RequiredSkills.Count; i++)
                {
                    ushort reqId = learn.RequiredSkills.Keys.ToArray()[i];
                    uint reqLevel = learn.RequiredSkills.Values.ToArray()[i];

                    Skill req = player.GetSkill(reqId);
                    if (req == null)
                    {
                        return;
                    }

                    if (req.Level < reqLevel)
                    {
                        return;
                    }
                }
            }

            if (player.SkillPoints < 1)
            {
                return;
            }

            if (player.GetSkill(id) != null)
            {
                player.GetSkill(id).AddLevel();
            }
            else
            {
                player.AddSkill(Skill.Create((ushort)id));
            }
            player.SkillPoints--;
            WNetworkMessage outMessage = new WNetworkMessage();
            PlayerSkillsPacket.Add(outMessage, player);
            PlayerStatusPacket.Add(outMessage, player);
            connection.Send(outMessage);
        }

        private void ParseSkillUse(Connection connection, OnSkillUsePacket packet)
        {
            Player player = Game.Instance.GetPlayer(connection);
            Skill skillUsed = player.GetSkill(packet.skillId);
            if (skillUsed == null) return;

            if (!skillUsed.OnUse(player, packet.targetArea))
            {
                //Send ON COOLDOWN!
            }
        }

        public void ParseAddStatus(Connection connection, AddStatusPacket packet)
        {
            Player player = Game.Instance.GetPlayer(connection);
            player.AddStatus(packet.Value, packet.Type, false);            
        }

        public void ParseMessage(Connection connection, MessagePacket packet)
        {
            Player player = Game.Instance.GetPlayer(connection);
            string message = player.Name + ": " + packet.Text;
            Game.Instance.HandleMessage(player, message, packet.Type);
        }

        public void ParseTouch(Position pos, Connection connection)
        {
            Player player = Game.Instance.GetPlayer(connection);
            Game.Instance.HandleTouch(player, pos);
        }

        public void ParseWalk(Direction direction, Connection connection)
        {
            Player player = Game.Instance.GetPlayer(connection);
            if (player.LastStepTime + player.GetStepDuration() < DateTime.Now.Ticks)
            {
                Game.Instance.CreatureWalk(player, direction);
            }
        }

        public void SendCreatureAppear(Creature creature, Connection connection)
        {
            WNetworkMessage outMessage = new WNetworkMessage();
            TileAddCreaturePacket.Add(outMessage, creature.tile.position, creature);
            connection.Send(outMessage);
        }

        public void SendInitialPacket(Player player)
        {
            WNetworkMessage outMessage = new WNetworkMessage();
            MapDescriptionPacket.Add(player.connection, outMessage, player);
            EffectPacket.Add(outMessage, player.tile.position, Effect.Login);
            PlayerStatusPacket.Add(outMessage, player);
            PlayerSkillsPacket.Add(outMessage, player);
            SkillTreePacket.Add(outMessage, player);
            foreach (var inv in player.Inventory.GetSlotItems())
            {
                InventorySetSlotPacket.Add(outMessage, inv.Value, inv.Key);
            }
            player.connection.Send(outMessage, true);
        }
    }
}

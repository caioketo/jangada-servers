﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;
using World_Server.Items;
using World_Server.Creatures;

namespace World_Server
{
    public class Map
    {
        private static Map instance;
        public static Map Instance 
        {
            get
            {
                if (instance == null)
                {
                    new Map();
                }
                return instance;
            }
            set
            {
                instance = value;
            }
        }

        Dictionary<Position, Tile> tiles = new Dictionary<Position, Tile>();

        public Map()
        {
            Map.Instance = this;
        }

        public void Load()
        {
        }

        public Tile GetTile(Position position)
        {
            if (position == null)
            {
                return null;
            }

            if (tiles.ContainsKey(position))
            {
                return tiles[position];
            }
            else
            {
                return null;
            }
        }

        public Tile GetTile(int x, int y, int z)
        {
            return GetTile(
                new Position(x, y, z)
            );
        }

        public bool SetTile(Position position, Tile tile)
        {
            tile.position = position;
            tiles.Add(position, tile);
            return true;
        }

        public int GetCreaturesCountInArea(Position[] Area, uint CreatureId)
        {
            int count = 0;

            for (int i = 0; i < Area.Length; i++)
            {
                Tile tile = GetTile(Area[i]);
                if (tile == null) continue;
                foreach (Creature creature in tile.Creatures)
                {
                    if (creature.Id == CreatureId)
                    {
                        count++;
                    }
                }
            }

            return count;
        }

        public Creature[] GetCreaturesInArea(Position[] Area, uint CreatureId)
        {
            Creature[] creatures = new Creature[GetCreaturesCountInArea(Area, CreatureId)];
            int pos = 0;
            for (int i = 0; i < Area.Length; i++)
            {
                Tile tile = GetTile(Area[i]);
                if (tile == null) continue;
                foreach (Creature creature in tile.Creatures)
                {
                    if (creature.Id == CreatureId)
                    {
                        creatures[pos] = creature;
                        pos++;
                    }
                }
            }

            return creatures;
        }
    }
}

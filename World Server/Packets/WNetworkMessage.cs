﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;
using World_Server.Items;
using World_Server.Creatures;
using World_Server.Creatures.Specs;

namespace World_Server.Packets
{
    public class WNetworkMessage : NetworkMessage
    {
        public void AddItem(Item item)
        {
            //For tests
            AddUInt16(item.Info.SpriteId);
            AddGuid(item.guid);
            AddString(item.Info.Description);
            if (item is Container)
            {
                AddByte(1);
                Container container = (Container)item;
                AddByte(container.Volume);
                AddByte(container.ItemCount);
                for (byte i = 0; i < container.ItemCount; i++)
                {
                    AddItem(container.GetItem(i));
                }
            }
            else
            {
                AddByte(0);
            }
        }

        public void AddCreature(Creature creature)
        {
            AddUInt32(creature.Id);
            AddGuid(creature.guid);
            AddString(creature.Name);
            AddByte(Convert.ToByte(creature.Health / creature.MaxHealth * 100));
            AddByte((byte)creature.direction);

        }

        public void AddSkill(Skill skill)
        {
            AddUInt16(skill.Id);
            AddUInt32(skill.Level);
            if (skill.Info.Targetable)
            {
                AddByte(1);
                AddUInt32((uint)skill.Info.AreaEffect.Length);
                for (int y = 0; y < skill.Info.AreaEffect.Length; y++)
                {
                    AddUInt32((uint)skill.Info.AreaEffect[y].Length);
                    for (int x = 0; x < skill.Info.AreaEffect[y].Length; x++)
                    {
                        AddString(skill.Info.AreaEffect[y][x]);
                    }
                }
            }
            else
            {
                AddByte(0);
            }
        }

        public void AddSkillInfo(SkillInfo info)
        {
            AddUInt16(info.Id);
            AddUInt32(info.LevelRequired);
            AddUInt16(info.PositionInTree);
            AddUInt16((ushort)info.RequiredSkills.Count);
            if (info.RequiredSkills.Count > 0)
            {
                var reqSkills = info.RequiredSkills.Keys.ToArray();
                foreach (ushort reqId in reqSkills)
                {
                    AddUInt16(reqId);
                }
            }
        }
    }
}

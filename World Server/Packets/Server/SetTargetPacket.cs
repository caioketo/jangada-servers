﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;

namespace World_Server.Packets.Server
{
    public class SetTargetPacket
    {
        public static void Add(WNetworkMessage message, Position pos, byte stackPos, TargetType type)
        {
            message.AddByte((byte)MessageType.SetTarget);
            message.AddPosition(pos);
            message.AddByte(stackPos);
            message.AddByte((byte)type);
        }
    }
}

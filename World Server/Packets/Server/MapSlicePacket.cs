﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;

namespace World_Server.Packets.Server
{
    public class MapSlicePacket : MapPacket
    {
        public static void Add(Connection connection, WNetworkMessage message, Position fromPos, Position toPos)
        {
            if (fromPos.Y > toPos.Y)
            {
                message.AddByte((byte)MessageType.MapSliceNorth);
                AddMapDescription(connection, message, fromPos.X - 14, toPos.Y - 15, toPos.Z, 28, 1);
            }
            else if (fromPos.Y < toPos.Y)
            {
                message.AddByte((byte)MessageType.MapSliceSouth);
                AddMapDescription(connection, message, fromPos.X - 14, toPos.Y + 14, toPos.Z, 28, 1);
            }

            if (fromPos.X < toPos.X)
            { 
                message.AddByte((byte)MessageType.MapSliceEast);
                AddMapDescription(connection, message, toPos.X + 14, toPos.Y - 14, toPos.Z, 1, 28);
            }
            else if (fromPos.X > toPos.X)
            {
                message.AddByte((byte)MessageType.MapSliceWest);
                AddMapDescription(connection, message, toPos.X - 15, toPos.Y - 14, toPos.Z, 1, 28);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using World_Server.Items;
using World_Server.Creatures;

namespace World_Server.Packets.Server
{
    public class LootPacket
    {
        public static void Add(WNetworkMessage message, Loot loot)
        {
            message.AddByte((byte)MessageType.Loot);
            message.AddUInt32((uint)loot.Id);
            message.AddUInt32((uint)loot.Items.Count);

            foreach (LootItem lootItem in loot.Items)
            {
                message.AddUInt32((uint)lootItem.Id);
                message.AddItem(lootItem.item);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using World_Server.Items;

namespace World_Server.Packets.Server
{
    public class InventorySetSlotPacket
    {
        public static void Add(WNetworkMessage message, Item item, SlotType slot)
        {
            message.AddByte((byte)MessageType.InventorySetSlot);
            message.AddByte((byte)slot);
            message.AddItem(item);
        }
    }
}

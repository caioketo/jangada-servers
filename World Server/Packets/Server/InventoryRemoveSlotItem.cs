﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World_Server.Packets.Server
{
    public class InventoryRemoveSlotItem
    {
        public static void Add(WNetworkMessage message, SlotType slot)
        {
            message.AddByte((byte)MessageType.InventoryRemoveSlot);
            message.AddByte((byte)slot);
        }
    }
}

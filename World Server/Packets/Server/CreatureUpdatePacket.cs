﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;

namespace World_Server.Packets.Server
{
    public class CreatureUpdatePacket
    {
        public static void Add(WNetworkMessage message, Position pos, byte stackPos, byte newHp)
        {
            message.AddByte((byte)MessageType.CreatureUpdate);
            message.AddPosition(pos);
            message.AddByte(stackPos);
            message.AddByte(newHp);
            Console.WriteLine("Creature Update: " + newHp.ToString());
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;

namespace World_Server.Packets.Server
{
    public class TileRemoveThingPacket
    {
        public static void Add(WNetworkMessage message, Position fromPosition, byte fromStackPosition)
        {
            message.AddByte((byte)MessageType.TileRemoveThing);
            message.AddPosition(fromPosition);
            message.AddByte(fromStackPosition);
        }
    }
}

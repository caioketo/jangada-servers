﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World_Server.Packets.Server
{
    public class ContainerRemoveItemPacket
    {
        public static void Add(WNetworkMessage message, byte containerIndex, byte containerPos)
        {
            message.AddByte((byte)MessageType.ContainerRemoveItem);
            message.AddByte(containerIndex);
            message.AddByte(containerPos);
        }
    }
}

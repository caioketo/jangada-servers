﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using World_Server.Creatures;
using World_Server.Creatures.Specs;

namespace World_Server.Packets.Server
{
    public class PlayerSkillsPacket
    {
        public static void Add(WNetworkMessage message, Player player)
        {
            if (player.SkillsLearnt.Count > 0)
            {
                message.AddByte((byte)MessageType.PlayerSkillsPacket);
                message.AddUInt16((ushort)player.SkillsLearnt.Count);
                foreach (Skill skill in player.SkillsLearnt)
                {
                    message.AddSkill(skill);
                }
            }
        }
    }
}

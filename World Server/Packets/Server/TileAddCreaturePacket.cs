﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;
using World_Server.Creatures;

namespace World_Server.Packets.Server
{
    public class TileAddCreaturePacket
    {
        public static void Add(WNetworkMessage message, Position pos, Creature creature)
        {
            message.AddByte((byte)MessageType.TileAddCreature);
            message.AddPosition(pos);
            message.AddCreature(creature);
        }
    }
}

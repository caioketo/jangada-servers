﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World_Server.Packets.Server
{
    public class TextMessagePacket
    {
        public static void Add(WNetworkMessage message, TextMessageType type, string text)
        {
            message.AddByte((byte)MessageType.TextMessagePacket);
            message.AddByte((byte)type);
            message.AddString(text);
        }
    }
}

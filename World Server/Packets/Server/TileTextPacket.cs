﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;

namespace World_Server.Packets.Server
{
    public class TileTextPacket
    {
        public static void Add(WNetworkMessage message, Position position, string text, TextColor color)
        {
            message.AddByte((byte)MessageType.TileText);
            message.AddPosition(position);
            message.AddString(text);
            message.AddByte((byte)color);
        }
    }
}

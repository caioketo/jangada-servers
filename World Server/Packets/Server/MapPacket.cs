﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;
using World_Server.Items;
using World_Server.Creatures;

namespace World_Server.Packets.Server
{
    public class MapPacket
    {
        public static void AddMapDescription(Connection connection, WNetworkMessage message, double x, double y, double z, ushort width, ushort height)
        {
            //for (int zStep = (int)(z - 1); zStep < (z + 1); zStep++)
            //{
                for (int yStep = (int)y; yStep < (y + height); yStep++)
                {
                    for (int xStep = (int)x; xStep < (x + width); xStep++)
                    {
                        Tile tile = Map.Instance.GetTile(xStep, yStep, (int)z);
                        AddTileDescription(connection, message, tile);
                    }
                }
            //}
        }


        public static void AddTileDescription(Connection connection, WNetworkMessage message, Tile tile)
        {
            if (tile != null)
            {
                message.AddUInt16((ushort)tile.GetTotalItems());

                if (tile.Ground != null)
                {
                    message.AddItem(tile.Ground);
                }

                foreach (Item item in tile.Items)
                {
                    message.AddItem(item);
                }

                message.AddUInt16((ushort)tile.Creatures.Count);
                foreach (Creature creature in tile.Creatures)
                {
                    message.AddCreature(creature);
                }
                
            }
            else
            {
                message.AddUInt16(0);
            }
        }
    }
}

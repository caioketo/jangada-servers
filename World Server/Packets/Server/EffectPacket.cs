﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;

namespace World_Server.Packets.Server
{
    public class EffectPacket
    {
        public static void Add(WNetworkMessage message, Position position, Effect effect)
        {
            message.AddByte((byte)MessageType.Effect);
            message.AddPosition(position);
            message.AddByte((byte)effect);
        }
    }
}

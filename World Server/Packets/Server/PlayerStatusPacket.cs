﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using World_Server.Creatures;

namespace World_Server.Packets.Server
{
    public class PlayerStatusPacket
    {
        public static void Add(WNetworkMessage message, Player player)
        {
            message.AddByte((byte)MessageType.PlayerStatusPacket);
            message.AddUInt32(player.Health);
            message.AddUInt32(player.MaxHealth);
            message.AddUInt32(player.Experience);
            message.AddUInt16(player.Level);
            message.AddUInt32(player.SkillPoints);
            message.AddUInt32((uint)player.Info.Stat.Get(StatType.STR));
            message.AddUInt32((uint)player.Info.Stat.Get(StatType.DEX));
            message.AddUInt32((uint)player.Info.Stat.Get(StatType.INT));
            message.AddUInt32((uint)player.Info.Stat.Get(StatType.CONS));
            message.AddUInt32((uint)player.Info.Stat.Get(StatType.WIS));
            message.AddUInt32((uint)player.Info.Stat.Get(StatType.CHAR));
            message.AddUInt32((uint)player.Info.Stat.PointsAvaliable);
        }

    }
}

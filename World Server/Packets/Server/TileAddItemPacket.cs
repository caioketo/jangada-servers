﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;
using World_Server.Creatures;
using World_Server.Items;

namespace World_Server.Packets.Server
{
    public class TileAddItemPacket
    {
        public static void Add(WNetworkMessage message, Position pos, Item item)
        {
            message.AddByte((byte)MessageType.TileAddItem);
            message.AddPosition(pos);
            message.AddItem(item);
        }
    }
}

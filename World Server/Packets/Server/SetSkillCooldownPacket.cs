﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using World_Server.Creatures.Specs;

namespace World_Server.Packets.Server
{
    public class SetSkillCooldownPacket
    {
        public static void Add(WNetworkMessage message, Skill skill)
        {
            message.AddByte((byte)MessageType.SetSkillCooldown);
            message.AddUInt32(skill.Id);
            message.AddUInt32(skill.GetCooldown());
        }
    }
}

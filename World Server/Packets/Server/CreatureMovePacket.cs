﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;

namespace World_Server.Packets.Server
{
    public class CreatureMovePacket
    {
        public static void Add(WNetworkMessage message, Position fromPos, byte fromStackPos, Position toPos, uint duration)
        {
            message.AddByte((byte)MessageType.CreatureMove);
            message.AddPosition(fromPos);
            message.AddByte(fromStackPos);
            message.AddPosition(toPos);
            message.AddUInt32(duration);
        }
    }
}

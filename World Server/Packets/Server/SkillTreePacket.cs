﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using World_Server.Creatures;
using World_Server.Creatures.Specs;

namespace World_Server.Packets.Server
{
    public class SkillTreePacket
    {
        public static void Add(WNetworkMessage message, Player player)
        {
            message.AddByte((byte)MessageType.SkillTree);
            message.AddUInt16((ushort)player.Class.skillTree.Skills.Count);

            foreach (ushort skillId in player.Class.skillTree.Skills)
            {
                //message.AddUInt16(skillId);
                SkillInfo sInfo = SkillInfo.GetSkillInfo(skillId);
                message.AddSkillInfo(sInfo);
                /*message.AddUInt32(sInfo.LevelRequired);
                message.AddUInt16((ushort)sInfo.RequiredSkills.Count);
                if (sInfo.RequiredSkills.Count > 0)
                {
                    var reqSkills = sInfo.RequiredSkills.Keys.ToArray();
                    foreach (ushort reqId in reqSkills)
                    {
                        message.AddUInt16(reqId);
                    }
                }*/
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;

namespace World_Server.Packets.Server
{
    public class CreatureSpeakPacket
    {
        public static void Add(WNetworkMessage message, string text, Position pos, byte type)
        {
            message.AddByte((byte)MessageType.CreatureSpeak);
            message.AddPosition(pos);
            message.AddByte(type);
            message.AddString(text);
        }
    }
}

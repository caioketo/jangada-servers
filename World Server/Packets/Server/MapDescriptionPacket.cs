﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;
using World_Server.Creatures;

namespace World_Server.Packets.Server
{
    public class MapDescriptionPacket : MapPacket
    {
        public static void Add(Connection connection, WNetworkMessage message, Player player)
        {
            message.AddByte((byte)MessageType.MapDescriptionPacket);
            Position playerPosition = player.tile.position;
            message.AddPosition(playerPosition);
            if (playerPosition.X < 14)
            {
                AddMapDescription(connection, message, 0, playerPosition.Y - 15, playerPosition.Z, (ushort)playerPosition.X, 30);
            }
            else if (playerPosition.Y < 10)
            {
                AddMapDescription(connection, message, playerPosition.X - 15, 0, playerPosition.Z, 30, (ushort)playerPosition.Y);
            }
            else
            {
                AddMapDescription(connection, message, playerPosition.X - 20, playerPosition.Y - 20, playerPosition.Z, 40, 40);
            }

            /*if (player.Client == ClientType.Web)
            {
                AddMapDescription(connection, message, playerPosition.X - 16, playerPosition.Y - 15, playerPosition.Z, 32, 30);
            }
            else
            {
                AddMapDescription(connection, message, playerPosition.X - 14, playerPosition.Y - 10, playerPosition.Z, 28, 20);
            }*/
        }
    }
}

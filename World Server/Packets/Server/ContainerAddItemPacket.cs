﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using World_Server.Items;

namespace World_Server.Packets.Server
{
    public class ContainerAddItemPacket
    {
        public static void Add(WNetworkMessage message, byte containerIndex, Item item)
        {
            message.AddByte((byte)MessageType.ContainerAddItem);
            message.AddByte(containerIndex);
            message.AddItem(item);
        }
    }
}

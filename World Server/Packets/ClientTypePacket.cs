﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World_Server.Packets
{
    public enum ClientTypePacket : byte
    {
        LoginWorld = 1,
        MoveNorth = 2,
        MoveSouth = 3,
        MoveEast = 4,
        MoveWest = 5,
        TouchedPos = 6,
        Message = 7,
        AddStatus = 8,
        LootOption = 9,
        OnItemUseInContainer = 10,
        InventoryRemoveItem = 11,
        SkillUse = 12,
        LearnSkill = 13,
        CreatureAttack = 14,
        CreatureFollow = 15,
        OnItemUse = 16,
        Logged = 17
    }
}

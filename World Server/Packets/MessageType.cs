﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World_Server.Packets
{
    public enum MessageType : byte
    {
        LoginWorldPacket = 1,
        MapDescriptionPacket = 5,
        TextMessagePacket = 6,
        PlayerStatusPacket = 7,
        TileAddCreature = 8,
        MapSliceNorth = 9,
        MapSliceSouth = 10,
        MapSliceEast = 11,
        MapSliceWest = 12,
        CreatureMove = 13,
        TileRemoveThing = 14,
        Effect = 15,
        TileText = 16,
        CreatureUpdate = 17,
        InventorySetSlot = 18,
        SetTarget = 19,
        CreatureSpeak = 20,
        TileAddItem = 21,
        Loot = 22,
        ContainerAddItem = 23,
        ContainerRemoveItem = 24,
        InventoryRemoveSlot = 25,
        PlayerSkillsPacket = 26,
        SetSkillCooldown = 27,
        SkillTree = 28
    }
}

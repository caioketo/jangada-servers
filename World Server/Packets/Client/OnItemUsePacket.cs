﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;

namespace World_Server.Packets.Client
{
    public class OnItemUsePacket
    {
        Guid guid;

        public static OnItemUsePacket Parse(NetworkMessage message)
        {
            OnItemUsePacket packet = new OnItemUsePacket();
            packet.guid = message.GetGuid();
            return packet;
        }
    }
}

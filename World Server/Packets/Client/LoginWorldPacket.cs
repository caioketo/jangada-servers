﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;

namespace World_Server.Packets.Client
{
    public class LoginWorldPacket
    {
        public uint CharacterId;
        public ClientType Client;

        public static LoginWorldPacket Parse(NetworkMessage message)
        {
            LoginWorldPacket packet = new LoginWorldPacket();

            string client = message.GetString();

            if (client.Equals("WEB"))
            {
                packet.Client = ClientType.Web;
            }

            packet.CharacterId = message.GetUInt32();


            return packet;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;

namespace World_Server.Packets.Client
{
    public class MessagePacket
    {
        public string Text;
        public byte Type;

        public static MessagePacket Parse(NetworkMessage message)
        {
            MessagePacket packet = new MessagePacket();

            packet.Type = message.GetByte();
            packet.Text = message.GetString();

            return packet;
        }
    }
}

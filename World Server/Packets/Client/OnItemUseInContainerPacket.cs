﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;

namespace World_Server.Packets.Client
{
    public class OnItemUseInContainerPacket
    {
        public ContainerIndex containerIndex;
        public byte containerPos;

        public static OnItemUseInContainerPacket Parse(NetworkMessage message)
        {
            OnItemUseInContainerPacket packet = new OnItemUseInContainerPacket();

            packet.containerIndex = (ContainerIndex)message.GetByte();
            packet.containerPos = message.GetByte();

            return packet;
        }
    }
}

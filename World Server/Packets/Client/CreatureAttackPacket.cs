﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;

namespace World_Server.Packets.Client
{
    public class CreatureAttackPacket
    {
        public Guid guid;

        public static CreatureAttackPacket Parse(NetworkMessage message)
        {
            CreatureAttackPacket packet = new CreatureAttackPacket();
            packet.guid = message.GetGuid();
            return packet;
        }
    }
}

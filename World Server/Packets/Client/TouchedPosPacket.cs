﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;

namespace World_Server.Packets.Client
{
    public class TouchedPosPacket
    {
        public Position touchedPos;

        public static TouchedPosPacket Parse(NetworkMessage message)
        {
            TouchedPosPacket packet = new TouchedPosPacket();

            packet.touchedPos = message.GetPosition();

            return packet;
        }
    }
}

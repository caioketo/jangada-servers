﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;
using World_Server.Creatures.Specs;

namespace World_Server.Packets.Client
{
    public class OnSkillUsePacket
    {
        public uint skillId;
        public Position[] targetArea = null;

        public static OnSkillUsePacket Parse(NetworkMessage message)
        {
            OnSkillUsePacket packet = new OnSkillUsePacket();
            packet.skillId = message.GetUInt32();
            if (SkillInfo.GetSkillInfo((ushort)packet.skillId).Targetable)
            {
                int areaCount = (int)message.GetUInt32();
                packet.targetArea = new Position[areaCount];
                for (int i = 0; i < areaCount; i++)
                {
                    packet.targetArea[i] = message.GetPosition();
                }
            }

            return packet;
        }
    }
}

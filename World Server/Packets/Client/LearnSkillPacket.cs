﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;

namespace World_Server.Packets.Client
{
    public class LearnSkillPacket
    {
        public uint skillId;

        public static LearnSkillPacket Parse(NetworkMessage message)
        {
            LearnSkillPacket packet = new LearnSkillPacket();
            packet.skillId = message.GetUInt32();
            return packet;
        }
    }
}

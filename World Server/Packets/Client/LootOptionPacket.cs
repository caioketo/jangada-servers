﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;
using World_Server.Creatures;

namespace World_Server.Packets.Client
{
    public class LootOptionPacket
    {
        public static void Parse(NetworkMessage message, Connection connection)
        {
            int lootId = (int)message.GetUInt32();
            int lootItemId = (int)message.GetUInt32();
            int option = (int)message.GetUInt32();
            Player player = Game.Instance.GetPlayer(connection);
            Game.Instance.LootItemChoice(player, lootId, lootItemId, option);
        }
    }
}

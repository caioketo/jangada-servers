﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;

namespace World_Server.Packets.Client
{
    public class AddStatusPacket
    {
        public uint Value;
        public StatType Type;

        public static AddStatusPacket Parse(NetworkMessage message)
        {
            AddStatusPacket packet = new AddStatusPacket();

            packet.Type = (StatType)message.GetByte();
            packet.Value = message.GetUInt32();
            return packet;
        }
    }
}

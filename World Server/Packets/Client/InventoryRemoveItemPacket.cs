﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;

namespace World_Server.Packets.Client
{
    public class InventoryRemoveItemPacket
    {
        public byte Slot;
        public static InventoryRemoveItemPacket Parse(NetworkMessage message)
        {
            InventoryRemoveItemPacket packet = new InventoryRemoveItemPacket();
            packet.Slot = message.GetByte();
            return packet;
        }
    }
}

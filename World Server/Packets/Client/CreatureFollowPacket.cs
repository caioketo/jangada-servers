﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;

namespace World_Server.Packets.Client
{
    public class CreatureFollowPacket
    {
        public Guid guid;

        public static CreatureFollowPacket Parse(NetworkMessage message)
        {
            CreatureFollowPacket packet = new CreatureFollowPacket();
            packet.guid = message.GetGuid();
            return packet;
        }
    }
}

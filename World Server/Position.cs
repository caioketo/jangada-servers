﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World_Server
{
    public class Position : IEquatable<Position>
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        public Position(double x, double y, double z)
        {
            X = x;
            Y = y;
            Z = z;
        }

        public Position(Position pos)
        {
            X = pos.X;
            Y = pos.Y;
            Z = pos.Z;
        }

        public override bool Equals(object other)
        {
            return other is Position && Equals((Position)other);
        }

        public bool Equals(Position other)
        {
            return other.X == X && other.Y == Y && other.Z == Z;
        }
    }
}

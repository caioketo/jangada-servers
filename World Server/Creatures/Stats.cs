﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World_Server.Creatures
{
    public class Stats
    {
        private int Strenght;
        private int Dexterity;
        private int Constution;
        private int Intelligence;
        private int Wisdom;
        private int Charisma;
        public int PointsAvaliable = 0;

        private int GetStr()
        {
            return Strenght;
        }

        private int GetDex()
        {
            return Dexterity;
        }

        private int GetCons()
        {
            return Constution;
        }

        private int GetInt()
        {
            return Intelligence;
        }

        private int GetWis()
        {
            return Wisdom;
        }

        private int GetChar()
        {
            return Charisma;
        }

        public int Get(StatType type)
        {
            switch (type)
            {
                case StatType.STR:
                    return GetStr();
                case StatType.DEX:
                    return GetDex();
                case StatType.CONS:
                    return GetCons();
                case StatType.INT:
                    return GetInt();
                case StatType.WIS:
                    return GetWis();
                case StatType.CHAR:
                    return GetChar();
                default:
                    return 0;
            }
        }

        public void Add(int mod, StatType type)
        {
            switch (type)
            {
                case StatType.STR:
                    AddStr(mod);
                    break;
                case StatType.DEX:
                    AddDex(mod);
                    break;
                case StatType.CONS:
                    AddCons(mod);
                    break;
                case StatType.INT:
                    AddInt(mod);
                    break;
                case StatType.WIS:
                    AddWis(mod);
                    break;
                case StatType.CHAR:
                    AddChar(mod);
                    break;
                case StatType.ALL:
                    AddStr(mod);
                    AddDex(mod);
                    AddCons(mod);
                    AddInt(mod);
                    AddWis(mod);
                    AddChar(mod);
                    break;
            }
        }

        private void AddStr(int mod)
        {
            Strenght += mod;
        }

        private void AddDex(int mod)
        {
            Dexterity += mod;
        }

        private void AddCons(int mod)
        {
            Constution += mod;
        }

        private void AddInt(int mod)
        {
            Intelligence += mod;
        }

        private void AddWis(int mod)
        {
            Wisdom += mod;
        }

        private void AddChar(int mod)
        {
            Charisma += mod;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using World_Server.Packets;
using World_Server.Packets.Server;
using JSLib;

namespace World_Server.Creatures.Specs
{
    public class Skill
    {
        public ushort Id;
        public uint Level;
        public bool IsActive;
        private long LastUse = 0;
        
        public SkillInfo Info
        {
            get
            {
                return SkillInfo.GetSkillInfo(Id);
            }
        }

        private Skill(ushort id)
        {
            this.Id = id;
            this.Level = 1;
            this.IsActive = false;
        }

        public static Skill Create(ushort id)
        {
            return new Skill(id);
        }

        public uint GetCooldown()
        {
            return Info.Cooldown;
        }

        public bool OnUse(Player player, Position[] targetArea)
        {
            if (!this.CanUse()) return false;
            this.LastUse = DateTime.Now.Ticks / 10000;

            WNetworkMessage outMessage = new WNetworkMessage();
            List<Creature> targets = new List<Creature>();

            if (this.Info.AreaEffect != null)
            {
                int areaCount = 0;
                int centerPosX = 0;
                int centerPosY = 0;
                for (int y = 0; y < this.Info.AreaEffect.Length; y++)
                {
                    for (int x = 0; x < this.Info.AreaEffect[y].Length; x++)
                    {
                        if (this.Info.AreaEffect[y][x] == "1")
                        {
                            areaCount++;
                        }

                        if (this.Info.AreaEffect[y][x].ToUpper() == "X")
                        {
                            centerPosX = x;
                            centerPosY = y;
                        }
                    }
                }

                Position[] area = new Position[areaCount];
                areaCount = 0;
                for (int y = 0; y < this.Info.AreaEffect.Length; y++)
                {
                    for (int x = 0; x < this.Info.AreaEffect[y].Length; x++)
                    {
                        if (this.Info.AreaEffect[y][x] == "1")
                        {
                            double eX = (player.tile.position.X) + (x - centerPosX);
                            double eY = (player.tile.position.Y) + (y - centerPosY);
                            Position pos = new Position(eX, eY, player.tile.position.Z);
                            area[areaCount++] = pos;
                            if (Map.Instance.GetTile(pos).Creatures.Count > 0)
                            {
                                targets.Add(Map.Instance.GetTile(pos).GetTopCreature());
                            }
                        }
                    }
                }

                foreach (Position pos in area)
                {
                    EffectPacket.Add(outMessage, pos, this.Info.effect);
                }
            }
            else
            {
                if (targetArea == null)
                {
                    targets.Add(player.Target);
                }
            }

            switch (this.Info.Type)
            {
                case SkillType.Activable:
                    if (this.IsActive)
                    {
                        this.Info.Script.onDesactivate();
                    }
                    else
                    {
                        this.Info.Script.onActivate();
                    }
                    return true;
                case SkillType.Damage:
                    int dmg = this.Info.Script.getDamage();
                    foreach (Creature creature in targets)
                    {
                        Game.Instance.CreatureApplyDamage(player, creature, dmg);
                    }
                    return true;
                case SkillType.Castable:
                    this.Info.Script.onCast();
                    SetSkillCooldownPacket.Add(outMessage, this);
                    player.connection.Send(outMessage);
                    return true;
                case SkillType.Passive:
                    return false;
                default:
                    return false;
            }
        }

        public bool CanUse()
        {
            if (LastUse == 0) return true;

            if (this.LastUse + this.Info.Cooldown < (DateTime.Now.Ticks / 10000))
            {
                return true;
            }

            return false;
        }

        public void AddLevel()
        {
            Level++;
        }

    }
}

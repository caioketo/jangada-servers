﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World_Server.Creatures.Specs
{
    public class Classes
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public SkillTree skillTree { get; set; }
    }
}

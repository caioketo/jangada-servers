﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using World_Server.Utils;

namespace World_Server.Creatures.Specs
{
    public class ClassInfo
    {
        public ushort Id { get; set; }
        public string Description { get; set; }
        public SkillTree skillTree { get; set; }
        public Dictionary<AttributeType, IAttribute> AttributeDictionary = new Dictionary<AttributeType, IAttribute>();

        private static Dictionary<ushort, ClassInfo> classInfoDictionary = new Dictionary<ushort, ClassInfo>();

        public ClassInfo(ushort id)
        {
            this.Id = id;
            this.skillTree = new SkillTree();
            Dictionary<ushort, SkillInfo> dic = SkillInfo.GetDictionary();
            List<SkillInfo> list = dic.Values.ToList();
            IEnumerable<SkillInfo> skills = list.Where(
                skill => skill.Class == this.Id);
            var skillsIds = skills.Select(skill => skill.Id);
            skillTree.Skills = skillsIds.ToList();
        }

        public static ClassInfo GetClassInfo(ushort Id)
        {
            return classInfoDictionary[Id];
        }

        public static void SetDictionary(Dictionary<ushort, ClassInfo> dic)
        {
            classInfoDictionary = dic;
        }
    }
}

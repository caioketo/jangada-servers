﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using World_Server.Scripting;

namespace World_Server.Creatures.Specs
{
    public class SkillInfo
    {
        public ushort Id;
        public string Name;
        public string Description;
        public SkillType Type;
        public uint LevelRequired;
        public Dictionary<ushort, uint> RequiredSkills = new Dictionary<ushort, uint>();
        public uint Cooldown;
        public ISkill Script;
        public ushort Class;
        public ushort PositionInTree;
        public Effect effect;
        public string[][] AreaEffect = null;
        public bool Targetable = false;

        private static Dictionary<ushort, SkillInfo> skillInfoDictionary = new Dictionary<ushort, SkillInfo>();

        public static Dictionary<ushort, SkillInfo> GetDictionary()
        {
            return skillInfoDictionary;
        }

        public static SkillInfo GetSkillInfo(ushort Id)
        {
            return skillInfoDictionary[Id];
        }

        public static void SetDictionary(Dictionary<ushort, SkillInfo> dic)
        {
            skillInfoDictionary = dic;
        }

        public static void AddInfo(ushort id, SkillInfo info)
        {
            info.Id = id;
            skillInfoDictionary.Add(id, info);
        }
    }
}

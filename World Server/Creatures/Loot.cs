﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using World_Server.Items;

namespace World_Server.Creatures
{
    public class Loot
    {
        public List<Player> Looters;
        public List<LootItem> Items = new List<LootItem>();
        public int Id;

        public Loot(List<Item> items, List<Player> looters, int id)
        {
            Id = id;
            Looters = looters;
            int i = 0;
            foreach (Item item in items)
            {
                Items.Add(new LootItem(item, i));
                i++;
            }
        }

        private LootItem GetItem(int id)
        {
            foreach (LootItem item in Items)
            {
                if (item.Id == id)
                {
                    return item;
                }
            }

            return null;
        }

        public Item GetLootItem(int id)
        {
            LootItem lItem = GetItem(id);
            if (lItem == null) return null;

            return lItem.item;
        }

        public bool CheckItem(int id)
        {
            LootItem item = GetItem(id);

            if (item == null)
            {
                return false;
            }

            return (item.PlayersOption.Count == Looters.Count);
        }

        public Player GetWinner(int id)
        {
            LootItem item = GetItem(id);

            if (item == null)
            {
                return null;
            }

            Player winner = item.GetWinner();
            Items.Remove(item);
            return winner;
        }

        public void Choose(Player player, int id, int option)
        {
            LootItem item = GetItem(id);
            if (item == null)
            {
                return;
            }

            switch ((LootOption)option)
            {
                case LootOption.Need:
                    item.Need(player);
                    break;
                case LootOption.Greed:
                    item.Greed(player);
                    break;
                case LootOption.Pass:
                    item.Pass(player);
                    break;
            }            
        }
    }
}

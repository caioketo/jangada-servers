﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using World_Server.Items;

namespace World_Server.Creatures
{
    public class LootItem
    {
        public int Id;
        public Item item;
        public Dictionary<Player, int> PlayersOption = new Dictionary<Player, int>();

        public LootItem(Item _item, int id)
        {
            item = _item;
            Id = id;
        }

        public void Need(Player player)
        {
            if (!PlayersOption.ContainsKey(player))
            {
                PlayersOption.Add(player, (int)LootOption.Need);
            }
        }

        public void Greed(Player player)
        {
            if (!PlayersOption.ContainsKey(player))
            {
                PlayersOption.Add(player, (int)LootOption.Greed);
            }
        }

        public void Pass(Player player)
        {
            if (!PlayersOption.ContainsKey(player))
            {
                PlayersOption.Add(player, (int)LootOption.Pass);
            }
        }

        public Player GetWinner()
        {
            if (PlayersOption.ContainsValue((int)LootOption.Need))
            {
                double maxValue = 0;
                Player winner = null;
                Random rand = new Random();
                foreach (Player player in PlayersOption.Keys.ToList())
                {
                    if (PlayersOption[player] != (int)LootOption.Need)
                    {
                        continue;
                    }
                    double pValue = rand.NextDouble() * 100;
                    if (pValue > maxValue)
                    {
                        winner = player;
                    }
                }
                return winner;
            }
            else if (PlayersOption.ContainsValue((int)LootOption.Greed))
            {
                double maxValue = 0;
                Player winner = null;
                Random rand = new Random();
                foreach (Player player in PlayersOption.Keys.ToList())
                {
                    if (PlayersOption[player] != (int)LootOption.Greed)
                    {
                        continue;
                    }
                    double pValue = rand.NextDouble() * 100;
                    if (pValue > maxValue)
                    {
                        winner = player;
                    }
                }
                return winner;
            }
            else
            {
                return null;
            }
        }
    }
}

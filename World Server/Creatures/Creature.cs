﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using World_Server.Items;
using JSLib;

namespace World_Server.Creatures
{
    public delegate uint OnGetAttackSpeed(uint attackSpeed);

    public class Creature : Thing
    {
        public event OnGetAttackSpeed OnGetASHandler;
        public uint Id { get; set; }
        public Guid guid { get; set; }
        public string Name { get; set; }
        public Tile tile { get; set; }

        public uint Health { get; set; }
        public Direction direction { get; set; }
        public long LastStepTime { get; set; }
        public byte LastStepCost { get; set; }
        public ushort Speed { get; set; }
        private CreatureInfo info = new CreatureInfo();

        public Position[] Area;

        public Creature Target = null;
        public TargetType TargetType = TargetType.Attack;
        public List<Attacker> Attackers = new List<Attacker>();
        
        public long LastAttackTime { get; set; }

        public uint MaxHealth 
        {
            get
            {
                return GetMaxHealth();
            }
        }
        
        public CreatureInfo Info
        {
            get
            {
                if (Id == 1)
                {
                    return info;
                }
                else
                {
                    return CreatureInfo.GetCreatureInfo(Id);
                }
            }
        }

        public Creature(uint id)
        {
            Id = id;
            Name = CreatureInfo.GetCreatureInfo(id).Name;
            Health = MaxHealth;
            if (!IsPlayer)
            {
                Game.Instance.AddCreature(this);
            }
        }

        public Creature() { }

        public override ushort GetThingId()
        {
            return 0;
        }

        public bool IsPlayer
        {
            get
            {
                return this is Player;
            }
        }

        public uint GetMaxHealth()
        {
            uint maxHealth = 0;
            maxHealth += (uint)Info.Stat.Get(StatType.CONS) * 10;

            return maxHealth;
        }

        public uint GetStepDuration()
        {
            return 2000;
            //uint duration = 0;            
            //if (tile.Ground != null)
            //{
                //uint groundSpeed = tile.Ground.Info.Speed;
                //if (groundSpeed <= 0)
                    //groundSpeed = 100;
                //uint stepSpeed = Speed;
                //if (stepSpeed > 0)
                //{
                    //duration = (1000 * groundSpeed) / stepSpeed;
                //}
            //}

            //return duration * LastStepCost;
        }

        public bool ContainPlayer(Player player)
        {
            Attacker attacker = Attackers.Where(a => a.player == player).FirstOrDefault();
            return attacker != null;
        }

        public Attacker GetAttacker(Player player)
        {
            return Attackers.Where(a => a.player == player).FirstOrDefault();
        }

        public List<Player> GetAttackers()
        {
            List<Attacker> attackers = Attackers.Where(a => a.lastHitInMSec >= ((DateTime.Now.Ticks / 10000) - 30000)).ToList();
            List<Player> result = new List<Player>();
            foreach (Attacker attacker in attackers)
            {
                result.Add(attacker.player);
            }

            return result;
        }

        public int ApplyDamage(int damage, Creature attacker)
        {
            if (attacker.IsPlayer)
            {
                //Fazer algo para apagar depois de um tempo, ou dps que regenerar hp
                if (!ContainPlayer((Player)attacker))
                {
                    Attackers.Add(new Attacker((Player)attacker));
                }
                else
                {
                    GetAttacker((Player)attacker).Update();
                }
            }
            int oldHealth = (int)Health;
            if (Health <= damage)
            {
                Health = 0;
                OnDie();
                return oldHealth;
            }
            else
            {
                Health = (ushort)(Health - damage);
                if (Health <= 0)
                {
                    OnDie();
                }
                return damage;
            }
        }

        public void OnDie()
        {
            Game.Instance.CreatureDie(this);
        }

        public int GetDamage()
        {
            // DMG here
            return 20;
        }

        public uint GetAttackDuration()
        {
            // AS goes here
            uint baseAS = 5000;
            if (OnGetASHandler != null)
            {
                foreach (OnGetAttackSpeed onGetAS in OnGetASHandler.GetInvocationList())
                {
                    baseAS += (uint)onGetAS.DynamicInvoke(baseAS);
                }
            }

            if (this.IsPlayer)
            {
                Player player = (Player)this;
                if (player.Class.AttributeDictionary.ContainsKey(AttributeType.AttackSpeed))
                {
                    int mod = player.Class.AttributeDictionary[AttributeType.AttackSpeed].GetModifier(baseAS);
                    mod += (int)baseAS;
                    baseAS = (uint)mod;
                }

                if (player.Race.AttributeDictionary.ContainsKey(AttributeType.AttackSpeed))
                {
                    int mod = player.Race.AttributeDictionary[AttributeType.AttackSpeed].GetModifier(baseAS);
                    mod += (int)baseAS;
                    baseAS = (uint)mod;
                }
            }

            Console.WriteLine("AS " + baseAS.ToString());
            return baseAS;
        }


        public Position[] GetSeeArea()
        {
            Position[] area = new Position[660];
            int pos = 0;
            for (int y = (int)this.tile.position.Y - 10; y < (int)this.tile.position.Y + 11; y++)
            {
                for (int x = (int)this.tile.position.X - 14; x < (int)this.tile.position.X + 15; x++)
                {
                    area[pos] = new Position(x, y, 1);
                    pos++;
                }
            }
            return area;
        }

        public void Update()
        {
            if (this.IsPlayer)
            {
                if (this.Target != null && this.tile.position.isInRange(Target.tile.position, this.GetRange()))
                {
                    //Attack
                    if (this.LastAttackTime + this.GetAttackDuration() < (DateTime.Now.Ticks / 10000))
                    {
                        this.LastAttackTime = DateTime.Now.Ticks / 10000;
                        Game.Instance.CreatureAttack(this, this.Target);
                    }
                }
                return;
            }


            //DO AI work here...
            if (Target == null)
            {
                // Change Area for SeeArea
                if (Map.Instance.GetCreaturesCountInArea(GetSeeArea(), 1) > 0)
                {
                    Creature[] playersInArea = Map.Instance.GetCreaturesInArea(GetSeeArea(), 1);

                    double distance = 50;

                    foreach (Creature player in playersInArea)
                    {
                        int x = ((int)player.tile.position.X) - ((int)this.tile.position.X);
                        int y = ((int)player.tile.position.Y) - ((int)this.tile.position.Y);
                        double dist = Math.Sqrt(x * x + y * y);
                        if (dist < distance)
                        {
                            distance = dist;
                            this.Target = player;
                        }
                    }
                }
            }

            if (Target == null)
            {
                //Walk
                if (this.LastStepTime + this.GetStepDuration() < DateTime.Now.Ticks / 10000)
                {
                    this.LastStepTime = DateTime.Now.Ticks / 10000;
                    Random rnd = new Random(DateTime.Now.Millisecond);
                    int dir = rnd.Next(0, 4);
                    while (!isWalkArea(dir))
                    {
                        dir = rnd.Next(0, 4);
                    }
                    Game.Instance.CreatureWalk(this, (Direction)dir);
                }
            }
            else
            {
                //Attack or Walk to the target
                if (this.tile.position.isInRange(Target.tile.position, this.GetRange()))
                {
                    //Attack
                    if (this.LastAttackTime + this.GetAttackDuration() < DateTime.Now.Ticks / 10000)
                    {
                        this.LastAttackTime = DateTime.Now.Ticks / 10000;
                        Game.Instance.CreatureAttack(this, this.Target);
                    }
                }
                else
                {
                    //Walk
                    if (this.LastStepTime + this.GetStepDuration() < DateTime.Now.Ticks / 10000)
                    {
                        this.LastStepTime = DateTime.Now.Ticks / 10000;
                        Game.Instance.CreatureWalk(this, getTargetDirection());
                    }
                }
            }
        }


        public bool isWalkArea(int dir)
        {
            int x = (int)this.tile.position.X;
            int y = (int)this.tile.position.Y;
            switch ((Direction)dir)
            {
                case Direction.South:
                    y++;
                    break;
                case Direction.North:
                    y--;
                    break;
                case Direction.West:
                    x--;
                    break;
                case Direction.East:
                    x++;
                    break;
            }
            Position newPos = new Position(x, y, 1);
            return newPos.isInArea(Area);
        }

        public double GetRange()
        {
            double range = 1.5f;

            return range;
        }

        public Direction getTargetDirection()
        {
            Direction dir = 0;
            Random rnd = new Random(DateTime.Now.Millisecond);
            int walkX = 0;
            int walkY = 0;

            int cX = (int)this.tile.position.X;
            int cY = (int)this.tile.position.Y;
            int tX = (int)Target.tile.position.X;
            int tY = (int)Target.tile.position.Y;

            if (cX != tX && cX != (tX - 1) && cX != (tX + 1))
            {
                if (cX > tX)
                {
                    walkX = -1;
                }
                else
                {
                    walkX = 1;
                }
            }
            if (cY != tY && cY != (tY - 1) && cY != (tY + 1))
            {
                if (cY > tY)
                {
                    walkY = -1;
                }
                else
                {
                    walkY = 1;
                }
            }

            if (walkX != 0 && walkY != 0)
            {

                double rand = rnd.Next(0, 10);
                if (rand >= 5)
                {
                    walkX = 0;
                }
                else
                {
                    walkY = 0;
                }
            }

            if (walkX != 0)
            {
                if (walkX > 0)
                {
                    dir = Direction.East;
                }
                else
                {
                    dir = Direction.West;
                }
            }
            else if (walkY != 0)
            {
                if (walkY > 0)
                {
                    dir = Direction.South;
                }
                else
                {
                    dir = Direction.North;
                }
            }

            bool correctDir = false;
            while (!correctDir)
            {
                int newX = (int)tile.position.X;
                int newY = (int)tile.position.Y;
                switch (dir)
                {
                    case Direction.South:
                        newY++;
                        break;
                    case Direction.North:
                        newY--;
                        break;
                    case Direction.West:
                        newX--;
                        break;
                    case Direction.East:
                        newX++;
                        break;
                }
                Position newPos = new Position(newX, newY, 1);
                Tile newTile = Map.Instance.GetTile(newPos);

                if (newTile.IsWalkable())
                {
                    correctDir = true;
                    break;
                }

                if ((dir == Direction.South || dir == Direction.North) && !newTile.IsWalkable())
                {
                    if (cX == tX)
                    {
                        dir = Direction.East;
                    }
                    else
                    {
                        if (cX == (tX + 1))
                        {
                            dir = Direction.West;
                        }
                        else if (cX == (tX - 1))
                        {
                            dir = Direction.East;
                        }
                        else
                        {
                            if (cX > tX)
                            {
                                dir = Direction.West;
                            }
                            else
                            {
                                dir = Direction.East;
                            }
                        }
                    }
                }

                if ((dir == Direction.East || dir == Direction.West) && !newTile.IsWalkable())
                {
                    if (cY == tY)
                    {
                        dir = Direction.North;
                    }
                    else
                    {
                        if (cY == (tY + 1))
                        {
                            dir = Direction.South;
                        }
                        else if (cY == (tY - 1))
                        {
                            dir = Direction.South;
                        }
                        else
                        {
                            if (cY > tY)
                            {
                                dir = Direction.North;
                            }
                            else
                            {
                                dir = Direction.South;
                            }
                        }
                    }
                }
            }

            return dir;
        }
    }
}

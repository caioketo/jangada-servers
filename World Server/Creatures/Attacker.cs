﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World_Server.Creatures
{
    public class Attacker
    {
        public Player player;
        public long lastHitInMSec;

        public Attacker(Player _player)
        {
            player = _player;
            lastHitInMSec = DateTime.Now.Ticks / 10000;
        }

        public void Update()
        {
            lastHitInMSec = DateTime.Now.Ticks / 10000;
        }
    }
}

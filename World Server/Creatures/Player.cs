﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;
using World_Server.Items;
using World_Server.Utils;
using World_Server.Packets;
using World_Server.Packets.Server;
using World_Server.Creatures.Specs;

namespace World_Server.Creatures
{
    public class Player : Creature
    {
        public int CharacterId;
        public Connection connection;
        public ushort Level;
        public Position SavedPosition;
        public Inventory Inventory;
        public uint Experience;
        public uint SkillPoints;
        public List<Skill> SkillsLearnt = new List<Skill>();
        public ClassInfo Class;
        public RaceInfo Race;
        public ClientType Client;
        public bool isLogged { get; set; }

        public PlayerInfo GetInfo()
        {
            PlayerInfo info = new PlayerInfo();
            info.CharacterId = this.CharacterId;
            info.Name = this.Name;
            info.Level = this.Level;
            info.SavedPosition = this.tile.position;
            info.Str = this.Info.Stat.Get(StatType.STR);
            info.Dex = this.Info.Stat.Get(StatType.DEX);
            info.Int = this.Info.Stat.Get(StatType.INT);
            info.Cons = this.Info.Stat.Get(StatType.CONS);
            info.Wis = this.Info.Stat.Get(StatType.WIS);
            info.Char = this.Info.Stat.Get(StatType.CHAR);
            info.Experience = this.Experience;

            info.Inventory = new InventoryInfo();
            foreach (var i in this.Inventory.GetSlotItems())
            {
                info.Inventory.Items.Add((byte)i.Key, i.Value.Id);
            }

            info.SkillsLearnt = new Dictionary<ushort,uint>();

            foreach (Skill s in this.SkillsLearnt)
            {
                info.SkillsLearnt.Add(s.Id, s.Level);
            }

            return info;
        }

        public Player(PlayerInfo info)
        {
            this.isLogged = false;
            this.CharacterId = info.CharacterId;
            this.Id = 1;
            this.Name = info.Name;
            this.Level = info.Level;
            this.SavedPosition = info.SavedPosition;
            if (info.SavedPosition != null)
            {
                this.tile = Map.Instance.GetTile(info.SavedPosition);
            }
            this.LastStepCost = 1;
            this.LastStepTime = DateTime.Now.Ticks;
            this.Info.Stat = new Stats();
            this.Info.Stat.Add(info.Str, StatType.STR);
            this.Info.Stat.Add(info.Dex, StatType.DEX);
            this.Info.Stat.Add(info.Int, StatType.INT);
            this.Info.Stat.Add(info.Cons, StatType.CONS);
            this.Info.Stat.Add(info.Wis, StatType.WIS);
            this.Info.Stat.Add(info.Char, StatType.CHAR);
            this.Inventory = new Inventory();
            this.Health = this.MaxHealth;
            this.Experience = info.Experience;
            this.SkillPoints = info.SkillPoints;
            this.Class = ClassInfo.GetClassInfo(info.ClassId);
            this.Race = RaceInfo.GetRaceInfo(info.RaceId);
            for (byte i = (byte)SlotType.First; i < (byte)SlotType.Last; i++)
            {
                if (info.Inventory.Items.ContainsKey(i))
                {
                    this.Inventory.SetItemInSlot(Item.Create(info.Inventory.Items[i]), (SlotType)i);
                }
            }

            for (int i = 0; i < info.SkillsLearnt.Count; i++)
            {
                Skill s = Skill.Create(info.SkillsLearnt.ElementAt(i).Key);
                s.Level = info.SkillsLearnt.ElementAt(i).Value;
                this.AddSkill(s);
            }
        }

        public bool CanLearn(Skill skill)
        {
            if (this.SkillsLearnt.Contains(skill))
            {
                return true;
            }

            var skillsIds = skill.Info.RequiredSkills.Keys.ToArray();
            var skillsLevels = skill.Info.RequiredSkills.Values.ToArray();

            for (int i = 0; i < skillsIds.Count(); i++)
            {
                uint skillId = skillsIds[i];
                Skill s = GetSkill(skillId);
                if (s == null)
                {
                    return false;
                }

                if (s.Level < skillsLevels[i])
                {
                    return false;
                }
            }
            
            return true;
        }

        public Skill GetSkill(uint id)
        {
            foreach (Skill skill in this.SkillsLearnt)
            {
                if (skill.Id == id)
                {
                    return skill;
                }
            }
            return null;
        }

        public void AddSkill(Skill skill)
        {
            SkillsLearnt.Add(skill);
            skill.Info.Script.onInit(skill, this);
        }

        public void AddExp(uint exp)
        {
            this.Experience += exp;
            uint nextLevelExp = Util.GetNextLevelExp(this.Level++);

            WNetworkMessage outMessage = new WNetworkMessage();
            TextMessagePacket.Add(outMessage, TextMessageType.Default, string.Format("You gained {0} experience!", exp));
            if (nextLevelExp > 0 && this.Experience > nextLevelExp)
            {
                this.Level++;
                TextMessagePacket.Add(outMessage, TextMessageType.Default, string.Format("You advanced from level {0} to {1}", this.Level - 1, this.Level));
            }
            PlayerStatusPacket.Add(outMessage, this);
            this.connection.Send(outMessage);
        }

        public void AddStatus(uint mod, StatType type, bool forces)
        {
            if ((this.Info.Stat.PointsAvaliable >= mod) || forces)
            {
                this.Info.Stat.Add((int)mod, type);

                if (!forces)
                {
                    this.Info.Stat.PointsAvaliable -= (int)mod;
                }

                WNetworkMessage outMessage = new WNetworkMessage();
                PlayerStatusPacket.Add(outMessage, this);
                this.connection.Send(outMessage);
            }
        }

        public void RemoveStatus(int mod, StatType statType)
        {
            this.Info.Stat.Add(mod * -1, statType);
            WNetworkMessage outMessage = new WNetworkMessage();
            PlayerStatusPacket.Add(outMessage, this);
            this.connection.Send(outMessage);
        }

        public void DoFollow()
        {
            if ((this.TargetType != TargetType.Follow) || (this.Target == null))
            {
                return;
            }
            Creature followed = this.Target;
            if (!Position.IsInRange(this.tile.position, followed.tile.position, 1.5f))
            {
                Direction dir = this.getTargetDirection();
                Game.Instance.CreatureWalk(this, dir);
            }
            Scheduler.AddTask(this.DoFollow, null, (int)this.GetStepDuration());
        }
    }
}

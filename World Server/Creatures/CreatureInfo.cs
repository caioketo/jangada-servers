﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using World_Server.Items;

namespace World_Server.Creatures
{
    public class CreatureInfo
    {
        public Stats Stat = new Stats();
        public string Name;
        public ushort BodyId;
        public LootInfo loot = new LootInfo();
        public uint Experience;

        private static Dictionary<uint, CreatureInfo> creatureInfoDictionary = new Dictionary<uint, CreatureInfo>();

        public static CreatureInfo GetCreatureInfo(uint id)
        {
            return creatureInfoDictionary[id];
        }

        public static Dictionary<uint, CreatureInfo> GetDictionary()
        {
            return creatureInfoDictionary;
        }

        public static void SetDictionary(Dictionary<uint, CreatureInfo> dic)
        {
            creatureInfoDictionary = dic;
        }
    }
}

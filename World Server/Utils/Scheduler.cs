﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace World_Server.Utils
{
    public static class Scheduler
    {
        public delegate void Task();
        private static Object addEventLock = new Object();

        public static void AddTask(Task action, object[] paramArray, int delayInMilliseconds)
        {
            lock (addEventLock)
            {
                Action<Task, int> myDelegate = new Action<Task, int>(AddTaskDelay);
                myDelegate.BeginInvoke(action, delayInMilliseconds, null, null);
            }
        }

        private static void AddTaskDelay(Task action, int delayInMilliseconds)
        {
            System.Threading.Thread.Sleep(delayInMilliseconds);
            bool bFired;

            if (action != null)
            {
                foreach (Delegate singleCast in action.GetInvocationList())
                {
                    bFired = false;
                    try
                    {
                        ISynchronizeInvoke syncInvoke = (ISynchronizeInvoke)singleCast.Target;
                        if (syncInvoke != null && syncInvoke.InvokeRequired)
                        {
                            bFired = true;
                            syncInvoke.BeginInvoke(singleCast, null);
                        }
                        else
                        {
                            bFired = true;
                            singleCast.DynamicInvoke(null);
                        }
                    }
                    catch (Exception)
                    {
                        if (!bFired)
                            singleCast.DynamicInvoke(null);
                    }
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World_Server.Utils
{
    public class RespawnHandler
    {
        private static RespawnHandler instance;
        public static RespawnHandler Instance
        {
            get
            {
                if (instance == null)
                {
                    new RespawnHandler();
                }
                return instance;
            }
            set
            {
                instance = value;
            }
        }

        public RespawnHandler()
        {
            instance = this;
        }

        public Respawn[] Respawns;

        public void Update()
        {
            for (int i = 0; i < Respawns.Length; i++)
            {
                Respawns[i].Update();
            }
        }
    }
}

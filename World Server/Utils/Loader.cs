﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using JSLib;
using World_Server.Items;
using System.Xml;
using World_Server.Creatures;
using World_Server.Creatures.Specs;

namespace World_Server.Utils
{
    public class Loader
    {
        public static void LoadMap()
        {
            Map map = Map.Instance;

            FileStream fs = File.OpenRead(@"Resources\world.map");
            byte[] bytes = new byte[fs.Length];
            try
            {
                fs.Read(bytes, 0, Convert.ToInt32(fs.Length));
                fs.Close();
            }
            finally
            {
                fs.Close();
            }
            int m = 4;
            while (m < bytes.Length)
            {
                Tile tile = new Tile();
                tile.position = new Position(BitConverter.ToInt32(bytes, m), BitConverter.ToInt32(bytes, 4 + m), 1);

                tile.Ground = Item.Create((ushort)(BitConverter.ToUInt64(bytes, 8 + m)));
                int itemCount = (int)BitConverter.ToUInt32(bytes, 16 + m);
                for (int i = 0; i < itemCount; i++)
                {
                    tile.AddItem(Item.Create((ushort)(BitConverter.ToUInt64(bytes, (20 + (i*8) + m)))));
                }

                map.SetTile(tile.position, tile);

                m += 20 + (itemCount * 8);
            }
        }


        public static void LoadRespawns()
        {
            RespawnHandler handler = RespawnHandler.Instance;

            XmlTextReader reader = new XmlTextReader(@"Resources\respawn.xml"); 
            XmlDocument doc = new XmlDocument();
            doc.Load(reader);
            XmlNodeList respawnsNode = doc.SelectSingleNode("respawns").SelectNodes("respawn");
            handler.Respawns = new Respawn[respawnsNode.Count];
            int pos = 0;
            foreach (XmlNode node in respawnsNode)
            {
                Respawn resp = new Respawn();
                resp.CreatureId = UInt32.Parse(node.SelectSingleNode("creatureId").InnerText);
                resp.MaxCreatures = int.Parse(node.SelectSingleNode("maxCreatures").InnerText);
                resp.RespawnTime = long.Parse(node.SelectSingleNode("time").InnerText);
                int posX = int.Parse(node.SelectSingleNode("positionX").InnerText);
                int posY = int.Parse(node.SelectSingleNode("positionY").InnerText);
                resp.CenterPos = new Position(posX, posY, 1);

                int dist = int.Parse(node.SelectSingleNode("distance").InnerText);

                //Calculate Area
                int areaCount = 1;

                for (int a = 1; a <= dist; a++)
                {
                    areaCount += 4 * a;
                }

                Position[] temp = new Position[areaCount];
                int j = 0;
                int t = 0;
                int rY = posX;
                int rX = posY;
                for (int y = rY - dist; y < rY + dist; y++)
                {
                    for (int x = rX - j; x < rX + j + 1; x++)
                    {
                        temp[t] = new Position(x, y, 1);
                        t++;
                    }
                    if (y < rY)
                    {
                        j++;
                    }
                    else
                    {
                        j--;
                    }
                }
                Position[] area = new Position[t];
                for (int x = 0; x < t; x++)
                {
                    area[x] = temp[x];
                }
                resp.Area = area;
                handler.Respawns[pos] = resp;
                pos++;
            }
        }

        public static void LoadItems()
        {
            Dictionary<ushort, ItemInfo> dictionary = new Dictionary<ushort, ItemInfo>();

            XmlTextReader reader = new XmlTextReader(@"Resources\itemInfo.xml");
            XmlDocument doc = new XmlDocument();
            doc.Load(reader);
            XmlNodeList infoNode = doc.SelectSingleNode("items").SelectNodes("item");

            foreach (XmlNode node in infoNode)
            {
                ItemInfo info = new ItemInfo();
                info.SpriteId = ushort.Parse(node.SelectSingleNode("spriteId").InnerText);
                info.Speed = ushort.Parse(node.SelectSingleNode("speed").InnerText);
                info.Description = node.SelectSingleNode("description").InnerText;
                if (node.SelectSingleNode("decayTo") != null)
                {
                    info.DecayTo = ushort.Parse(node.SelectSingleNode("decayTo").InnerText);
                }

                if (node.SelectSingleNode("decayTime") != null)
                {
                    info.DecayTime = long.Parse(node.SelectSingleNode("decayTime").InnerText);
                }

                if (node.SelectSingleNode("volume") != null)
                {
                    info.Volume = byte.Parse(node.SelectSingleNode("volume").InnerText);
                }
                info.Group = (ItemGroup)int.Parse(node.SelectSingleNode("group").InnerText);
                if (node.SelectSingleNode("slot") != null)
                {
                    info.slot = (SlotType)byte.Parse(node.SelectSingleNode("slot").InnerText);
                }

                dictionary.Add(ushort.Parse(node.SelectSingleNode("id").InnerText), info);
            }

            ItemInfo.SetDictionary(dictionary);
        }

        public static void LoadCreatures()
        {
            Dictionary<uint, CreatureInfo> dictionary = new Dictionary<uint, CreatureInfo>();

            XmlTextReader reader = new XmlTextReader(@"Resources\creatureInfo.xml");
            XmlDocument doc = new XmlDocument();
            doc.Load(reader);
            XmlNodeList infoNode = doc.SelectSingleNode("creatures").SelectNodes("creature");

            foreach (XmlNode node in infoNode)
            {
                CreatureInfo info = new CreatureInfo();
                info.Name = node.SelectSingleNode("name").InnerText;
                info.BodyId = ushort.Parse(node.SelectSingleNode("bodyId").InnerText);
                info.Experience = uint.Parse(node.SelectSingleNode("experience").InnerText);
                XmlNode statNode = node.SelectSingleNode("stats");
                info.Stat.Add(int.Parse(statNode.SelectSingleNode("str").InnerText), StatType.STR);
                info.Stat.Add(int.Parse(statNode.SelectSingleNode("dex").InnerText), StatType.DEX);
                info.Stat.Add(int.Parse(statNode.SelectSingleNode("int").InnerText), StatType.INT);
                info.Stat.Add(int.Parse(statNode.SelectSingleNode("cons").InnerText), StatType.CONS);
                info.Stat.Add(int.Parse(statNode.SelectSingleNode("wis").InnerText), StatType.WIS);
                info.Stat.Add(int.Parse(statNode.SelectSingleNode("char").InnerText), StatType.CHAR);

                XmlNode lootsNode = node.SelectSingleNode("loots");
                foreach (XmlNode lootNode in lootsNode.SelectNodes("loot"))
                {
                    info.loot.Ids.Add(ushort.Parse(lootNode.SelectSingleNode("item").InnerText));
                    info.loot.Chances.Add(int.Parse(lootNode.SelectSingleNode("chance").InnerText));
                }

                dictionary.Add(uint.Parse(node.SelectSingleNode("id").InnerText), info);
            }

            CreatureInfo.SetDictionary(dictionary);
        }

        public static void LoadLevels()
        {
            Dictionary<ushort, uint> dic = new Dictionary<ushort, uint>();

            try
            {
                using (StreamReader sr = new StreamReader(@"Resources\levels.lvl"))
                {
                    string line = "";
                    while ((line = sr.ReadLine()) != null)
                    {
                        string level = line.Substring(0, line.IndexOf(';'));
                        string exp = line.Substring(line.IndexOf(';') + 1);
                        dic.Add(ushort.Parse(level), uint.Parse(exp));
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }

            Util.ExpByLevel = dic;
        }

        public static void LoadClasses()
        {
            Dictionary<ushort, ClassInfo> dictionary = new Dictionary<ushort, ClassInfo>();
            XmlTextReader reader = new XmlTextReader(@"Resources\classes.xml");
            XmlDocument doc = new XmlDocument();
            doc.Load(reader);
            XmlNodeList infoNode = doc.SelectSingleNode("classes").SelectNodes("class");

            foreach (XmlNode node in infoNode)
            {
                ClassInfo info = new ClassInfo(ushort.Parse(node.SelectSingleNode("id").InnerText));
                info.Description = node.SelectSingleNode("description").InnerText;
                XmlNode modifiesNode = node.SelectSingleNode("modifies");
                if (modifiesNode != null)
                {
                    foreach (XmlNode modifyNode in modifiesNode.SelectNodes("modify"))
                    {
                        AttributeType type = Util.ParseAttributeType(modifyNode.SelectSingleNode("type").InnerText);
                        int percent = int.Parse(modifyNode.SelectSingleNode("percent").InnerText);
                        info.AttributeDictionary.Add(type, new ModifyAttribute(percent));
                    }
                }

                dictionary.Add(info.Id, info);
            }

            ClassInfo.SetDictionary(dictionary);
        }

        public static void LoadRaces()
        {
            Dictionary<ushort, RaceInfo> dictionary = new Dictionary<ushort, RaceInfo>();
            XmlTextReader reader = new XmlTextReader(@"Resources\races.xml");
            XmlDocument doc = new XmlDocument();
            doc.Load(reader);
            XmlNodeList infoNode = doc.SelectSingleNode("races").SelectNodes("race");

            foreach (XmlNode node in infoNode)
            {
                RaceInfo info = new RaceInfo(ushort.Parse(node.SelectSingleNode("id").InnerText));
                info.Description = node.SelectSingleNode("description").InnerText;
                XmlNode modifiesNode = node.SelectSingleNode("modifies");
                if (modifiesNode != null)
                {
                    foreach (XmlNode modifyNode in modifiesNode.SelectNodes("modify"))
                    {
                        AttributeType type = Util.ParseAttributeType(modifyNode.SelectSingleNode("type").InnerText);
                        int percent = int.Parse(node.SelectSingleNode("percent").InnerText);
                        info.AttributeDictionary.Add(type, new ModifyAttribute(percent));
                    }
                }
                dictionary.Add(info.Id, info);
            }

            RaceInfo.SetDictionary(dictionary);
        }
    }
}

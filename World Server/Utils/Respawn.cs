﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;
using World_Server.Creatures;
using System.Diagnostics;

namespace World_Server.Utils
{
    public class Respawn
    {
        public Position[] Area;
        public Position[] WalkArea; //Usar ou ignorar?
        public Position CenterPos;
        public uint CreatureId;
        public int MaxCreatures;
        public long RespawnTime;
        private long LastTimeInMSec;
        
        public void Update()
        {
            long now = DateTime.Now.Ticks / 10000;
            if (now - LastTimeInMSec > RespawnTime)
            {
                int creaturesCount = Map.Instance.GetCreaturesCountInArea(Area, CreatureId);
                if (creaturesCount < MaxCreatures)
                {
                    Tile cTile = Map.Instance.GetTile(CenterPos);
                    if (Game.Instance.GetSpectatorPlayers(CenterPos).Count() > 0)
                    {
                        return;
                    }
                    if (Map.Instance.GetCreaturesCountInArea(Area, 1) <= 0)
                    {
                        if (cTile.Creatures.Count > 0)
                        {
                            for (int i = 0; i < Area.Length; i++)
                            {
                                if (Map.Instance.GetTile(Area[i]).Creatures.Count <= 0)
                                {
                                    Creature c = new Creature(CreatureId);
                                    c.Area = Area;
                                    c.tile = Map.Instance.GetTile(Area[i]);
                                    Map.Instance.GetTile(Area[i]).Creatures.Add(c);
                                    //Game.Instance.AddCreature(c);
                                    Console.WriteLine("Creature created (" + CreatureId + ") at x = " + c.tile.position.X + ", y = " + c.tile.position.Y);
                                    LastTimeInMSec = DateTime.Now.Ticks / 10000;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            Creature c = new Creature(CreatureId);
                            cTile.Creatures.Add(c);
                            c.Area = Area;
                            c.tile = cTile;                            
                            //Game.Instance.AddCreature(c);
                            Console.WriteLine("Creature created (" + CreatureId + ") at x = " + c.tile.position.X + ", y = " + c.tile.position.Y);
                            LastTimeInMSec = DateTime.Now.Ticks / 10000;
                        }
                    }
                }
                else
                {
                    LastTimeInMSec = DateTime.Now.Ticks / 10000;
                }
            }            
        }
    }
}

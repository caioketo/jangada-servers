﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World_Server.Utils
{
    public class Util
    {
        public static Dictionary<ushort, uint> ExpByLevel = new Dictionary<ushort, uint>();

        public static uint GetNextLevelExp(ushort level)
        {
            if (ExpByLevel.ContainsKey(level))
            {
                return ExpByLevel[level];
            }
            else
            {
                return 0;
            }
        }

        public static AttributeType ParseAttributeType(string attributeType)
        {
            switch (attributeType)
            {
                case "AttackDamage":
                    return AttributeType.AttackDamage;
                case "AttackSpeed" :
                    return AttributeType.AttackSpeed;
                default :
                    return AttributeType.None;
            }
        }
    }
}

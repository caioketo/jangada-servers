﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace World_Server.Utils
{
    public interface IAttribute
    {
        int GetModifier(uint value);
    }
}

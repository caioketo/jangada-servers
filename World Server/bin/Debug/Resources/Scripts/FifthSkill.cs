﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using World_Server;
using World_Server.Scripting;
using World_Server.Creatures;
using World_Server.Creatures.Specs;
using World_Server.Utils;

public class FifthSkill : ISkill
{
    Skill skill;
    Player caster;

    public void onInit(Skill skill, Player caster)
    {
        //onInit = Passives uses it
        this.skill = skill;
        this.caster = caster;
    }

    public void onCast()
    {
        //onCast = Castables
        caster.OnGetASHandler += AddAS;
        Scheduler.AddTask(this.RemoveStat, null, (int)5000);
    }

    public uint AddAS(uint baseAS)
    {
        return (uint)(baseAS / 0.1f);
    }

    private void RemoveStat()
    {
        caster.OnGetASHandler -= AddAS;
    }

    public void onActivate()
    {
        //onActivate = Activables
    }

    public void onDesactivate()
    {
        //onDesactivate = Activables 
    }

    public void onEnd()
    {
        //onEnd
    }

    public SkillInfo getInfo()
    {
        SkillInfo info = new SkillInfo();

        info.Description = "Fifth Skill";
        info.Id = 5;
        info.LevelRequired = 5;
        info.Cooldown = 10;
        info.Type = SkillType.Castable;
        info.Class = (ushort)ClassesIds.Warrior;
        info.PositionInTree = 2;
        info.RequiredSkills.Add(2, 1);
        return info;
    }


    public int getDamage()
    {
        return 0;
    }
}

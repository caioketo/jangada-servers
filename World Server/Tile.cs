﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using World_Server.Items;
using JSLib;
using World_Server.Creatures;

namespace World_Server
{
    public class Tile
    {
        public Position position { get; set; }
        public Item Ground { get; set; }
        public IEnumerable<Item> Items { get { return items.AsEnumerable(); } }
        public int ItemCount { get { return items.Count; } }

        public List<Creature> Creatures { get; set; }
        private LinkedList<Item> items = new LinkedList<Item>();

        public int GetTotalItems()
        {
            int count = 0;
            if (Ground != null)
            {
                count++;
            }
            count += items.Count;
            return count;
        }

        public void AddItem(Item item)
        {
            item.tile = this;
            items.AddFirst(item);
        }

        public void RemoveItem(Item item)
        {
            item.tile = null;
            items.Remove(item);
        }

        public Creature GetTopCreature()
        {
            if (Creatures.Count <= 0)
            {
                return null;
            }
            return Creatures[Creatures.Count - 1];
        }

        public Tile()
        {
            items = new LinkedList<Item>();
            Creatures = new List<Creature>();
        }

        public Thing GetThingAtStackPosition(byte stackPosition)
        {
            int n = -1;

            if (Ground != null)
            {
                ++n;
                if (stackPosition == n)
                {
                    return Ground;
                }
            }

            if (ItemCount > 0)
            {
                foreach (Item item in Items)
                {
                    n++;
                    if (stackPosition == n)
                    {
                        return item;
                    }
                }
            }

            if (Creatures.Count > 0)
            {
                foreach (Creature creature in Creatures)
                {
                    ++n;
                    if (stackPosition == n)
                    {
                        return creature;
                    }
                }
            }

            return null;
        }

        public byte GetStackPosition(Thing thing)
        {
            int n = -1;

            if (Ground != null)
            {
                if (thing == Ground)
                {
                    return 0;
                }
                ++n;
            }

            if (ItemCount > 0)
            {
                foreach (Item item in Items)
                {
                    ++n;
                    if (thing == item)
                    {
                        return (byte)n;
                    }
                }
            }

            if (Creatures.Count > 0)
            {
                foreach (Creature creature in Creatures)
                {
                    ++n;
                    if (thing == creature)
                    {
                        return (byte)n;
                    }
                }
            }

            throw new Exception("Thing not found in tile.");
        }

        public bool IsWalkable()
        {
            return true;
        }
    }
}

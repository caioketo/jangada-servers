﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using World_Server.Creatures.Specs;
using World_Server.Creatures;

namespace World_Server.Scripting
{
    public interface ISkill
    {
        void onInit(Skill skill, Player caster);
        void onCast();
        void onActivate();
        void onDesactivate();
        void onEnd();
        int getDamage();
        SkillInfo getInfo();
    }
}

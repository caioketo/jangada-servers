﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JSLib;
using World_Server.Creatures;

namespace World_Server.Scripting
{
    //Login
    public delegate bool BeforeLoginHandler(Connection connection, string playerName);
    public delegate void AfterLoginHandler(Player player);
}
